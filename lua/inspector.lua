box.cfg { listen = 3301 }

box.on('database', function()
    local s = box.schema.space.create('selector')
    s:create_index('url', {parts = {1, 'string'}})
    s:create_index('selector', {parts = {2, 'string'}})
    s:create_index('name', {parts = {3, 'string'}})
end)

local function selector(self, req)
    if 'POST' == req.method then
        local url = string.sub(req.path, 2)
        --for k, v in req.pos
        box.space.selector:insert {url}
    end
    return req:render { json = { result = box.space.selector:select()  } }
end

local server = require('http.server').new(nil, 8080, { handler = selector })
server:start()
