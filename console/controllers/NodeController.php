<?php

namespace app\console\controllers;


use app\helpers\SQL;
use app\models\Node;
use app\models\Program;
use Yii;
use yii\console\Controller;

class NodeController extends Controller
{
    public function actionSeed() {
        $nodes = [];
        for ($i = 1; $i <= 6; $i++) {
            $nodes[] = [$i, $i, 1];
        }
        Yii::$app->db
            ->createCommand()
            ->batchInsert('node', ['id', 'program', 'user'], $nodes)
            ->execute();
    }

    public function actionPut($program_id = 1, $user_id = 1) {
        $parent_id = SQL::scalar('SELECT id FROM need WHERE program = :program_id', [':program_id' => $program_id]);
        SQL::execute('INSERT INTO node ("user", "parent", "program") VALUES (:user_id, :parent_id, :program_id)', [
            ':user_id' => $user_id,
            ':parent_id' => $parent_id,
            ':program_id' => $program_id,
        ]);
    }

    public function actionAdd($user_id = 1, $amount = 2500, $program_id = 11) {
        $node = new Node(['user' => $user_id, 'program' => $program_id]);
        $node->insert();
        SQL::execute('INSERT INTO transfer ("from", "amount", "type", "status", "node") VALUES (:user_id, :amount, :type, :status, :node)', [
            ':user_id' => $user_id,
            ':amount' => $amount,
            ':type' => 'buy',
            ':status' => 'success',
            ':node' => $node->id
        ]);
    }

    public function actionBuy($program_id = 1, $user_id = 1) {
        $tx = Yii::$app->db->beginTransaction();
        $node_id = Program::buy($program_id, $user_id, true);
        if ($node_id > 0) {
            $tx->commit();
        } else {
            $tx->rollBack();
        }
        echo "$node_id\n";
    }

    public function actionAccrue() {
        SQL::execute('SELECT accrue()');
    }
}
