<?php

namespace app\console\controllers;


use Yii;
use yii\console\Controller;

class BlockioController extends Controller {
    public function actionAddress() {
        $address = Yii::$app->blockio->getAddress('oliver');
        echo $address;
    }
}
