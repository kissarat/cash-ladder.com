WebFont.load({
  google: {
    families: [
      'Oswald:200,300,400,500,600,700',
      'Droid Sans:400,700',
      'Droid Serif:400,400italic,700,700italic',
      'Roboto:300,regular,500'
    ]
  }
})

function fetchJSON(url) {
  return fetch(url)
      .then(function (r) {
        return r.json()
      })
}

const vue = new Vue({
  el: '#app',
  data: {
    deposits: [],
    amount: 1000,
    system: null,
    children: [],
    ticker: 0,
    id: 0,
    expires: new Date('2030-11-16T16:01:07.250Z'),
    now: new Date(),
    days: 'Загрузка...',

    menu: {
      active: !!+localStorage.getItem('menu.active')
    },
  },

  computed: {
    euro: function () {
      return this.amount / 100
    },

    min: function () {
      const d = this.deposit
      return d ? d.min / 100 : 0
    },

    deposit: function () {
      const id = this.id
      return this.deposits.find(function (d) {
        return d.id === id
      })
    },

    profit: function () {
      const d = this.deposit
      return d ? +(this.amount * (1 + d.quantity * d.accrue)).toFixed(2) / 100 : ''
    },

    finish: function () {
      const d = this.deposit
      if (d) {
        const m = /^(.*)T/.exec(new Date(Date.now() + d.quantity * 24 * 3600 * 1000).toISOString())
        if (m) {
          return m[1]
        }
      }
      return ''
    },

    valid: function () {
      const d = this.deposit
      return d && this.amount >= d.min
    }
  },

  methods: {
    pay: function (system) {
      this.system = system
      $('#form-payment').submit()
    },

    select: function (id) {
      this.id = id
      this.amount = this.deposit.min
    },

    setEuro: function (cent) {
      this.amount = cent * 100
    },

    setProgramNode: function (id) {
      const self = this
      fetchJSON('/program/user?id=' + id)
          .then(function (r) {
            self.children = r.result
          })
    }
  }
})

$('[href="' + location.pathname + '"]').addClass('active')

$('<div class="overlay">' +
    '<img src="/images/under-construction.png" /><div class="text">Under Construction</div></div>')
    .appendTo('.under-construction')

$('[data-construction]').each(function (i, c) {
  c.addEventListener(c.getAttribute('data-construction'), function (e) {
    e.preventDefault()
    alert(t('Sorry. This function is under construction'))
  })
})

$('.eur').each(function (i, money) {
  const v = money.innerHTML / 100
  money.innerHTML = Number.isInteger(v) ? v : v.toFixed(2)
})

$('.autosubmit').submit()

$('.programs form').on('submit', function () {
  $('.programs button').remove()
})

// if ('serviceWorker' in navigator) {
//   navigator.serviceWorker
//       .register('/service.js', {scope: '/'})
// }

document.addEventListener('DOMContentLoaded', function () {
  if ('undefined' !== typeof deposits) {
    vue.deposits = deposits
  }
  $('[name=reflink]').click(function () {
    if ('function' === typeof document.execCommand) {
      document.querySelector('[name=reflink]').select()
      document.execCommand('copy')
    }
  })
})

$('[data-time]').each(function (i, el) {
  el.innerHTML = new Date(1000 * el.getAttribute('data-time')).toLocaleString();
})
var $all
var $$
(function ($d) {
  $all = $d.querySelectorAll.bind($d)
  $$ = $d.querySelector.bind($d)
})(document)

// function repeatEvery(ms, cb) {
//   return repeatEvery[ms] = setInterval(cb, ms)
// }
//
var tickerNumber = 0

setInterval(function () {
  vue.ticker = (tickerNumber++) % 10
}, 8000)

var element = $$('[name="expires"]');
element && (vue.expires = new Date(element.value))
&& (vue.days = Math.floor((Date.now() - new Date(element.value).getTime()) / (24 * 3600 * 1000)))
