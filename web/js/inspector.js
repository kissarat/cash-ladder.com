(function () {
  function *$combine(...seq) {
    for (let i = 1; i <= seq.length; i++) {
      for (let j = 0; j <= seq.length - i; j++) {
        yield seq.slice(j, j + i)
      }
    }
  }

  const denyAttrs = ['class', 'style', 'width', 'height', 'value']

  const rareTags = ['form',
    'footer',
    'header', 'article', 'section', 'aside', 'img', 'cite', 'optgroup', 'tbody', 'tr', 'td',
    'table', 'acronym', 'address', 'ul', 'ins', 'fieldset', 'abbr', 'rel', 'wbr', 'svg']

  function *y(el) {
    const tagName = el.tagName.toLowerCase()
    const names = [].filter.call(el.attributes, a => a.value && denyAttrs.indexOf(a.name) < 0
    && a.name.indexOf(':') < 0
    && a.value.indexOf('{') < 0)
    for (const attrs of $combine(...names)) {
      yield attrs.map(a => `[${a.name}="${a.value}"]`).join('')
    }
    for (const c of $combine(...el.classList.values())) {
      yield c.map(cc => '.' + cc).join('')
    }
    const count = el.parentNode.childElementCount
    if (count > 1) {
      let i
      switch (tagName) {
        case 'td':
          i = el.cellIndex + 1
          break
        case 'tr':
          i = el.rowIndex + 1
          break
        default:
          i = [].indexOf.call(el.parentNode.children, el) + 1
          break
      }
      if (i > 0) {
        yield tagName + `:nth-child(${i})`
      }
    }
    if (rareTags.indexOf(tagName) >= 0) {
      yield tagName
    }
  }

  function $check(selector) {
    return document.querySelectorAll(selector).length === 1 ? selector : false
  }

  function * $identity(el) {
    if (el.id) {
      yield '#' + el.id
    }

    for (const parts of y(el)) {
      yield parts
    }
  }

  function * deep(array, ...head) {
    if (head.length > 0) {
      for (const a of array) {
        for (const b of deep(...head)) {
          yield [a, ...b]
        }
      }
    }
    else {
      for (const a of array) {
        yield [a]
      }
    }
  }

  function * parental(el) {
    let parent = el
    while ((parent = parent.parentNode) && parent !== document.documentElement) {
      yield parent
    }
  }

  function checkElement(el) {
    for (const selector of $identity(el)) {
      if ($check(selector)) {
        return selector
      }
    }
  }

  function * group(...parents) {
    for (let i = 1; i <= parents.length; i++) {
      for (const comb of deep(...(parents.slice(0, i).reverse().map($identity)))) {
        yield comb.join(' ')
        // if (comb.length > 1) {
        //   yield comb.join('>')
        // }
      }
    }
  }

  function $selector(el) {
    for (const selector of group(el, ...parental(el))) {
      // console.log(selector)
      if ($check(selector)) {
        return selector
      }
    }
  }

  if (+localStorage.getItem('inspector')) {
    addEventListener('load', function () {
      const zen = document.createElement('div')
      zen.id = 'zen'
      const informer = document.createElement('div')
      const input = document.createElement('input')
      informer.appendChild(input)
      zen.appendChild(informer)
      document.body.appendChild(zen)

      function mouse(e) {
        const el = document.elementFromPoint(e.x, e.y)
        const selector = $selector(el)
        if (selector) {
          const el = document.querySelector(selector)
          const rect = el.getBoundingClientRect()
          // const style = getComputedStyle(el)
          // informer.style.width = style.width;
          if (e.y < 70) {
            informer.style.top = '0px'
          }
          else {
            informer.style.top = '-28px'
          }
          input.value = selector
          Object.assign(zen.style, {
            left: rect.left - 1 + 'px',
            top: rect.top - 1 + 'px',
            width: rect.width + 'px',
            height: rect.height + 'px'
          })
        }
        return selector
      }

      addEventListener('mousemove', mouse)
      addEventListener('click', function (e) {
        mouse(e)
        if ('function' === typeof document.execCommand) {
          document.querySelector('[name=reflink]').select()
          document.execCommand('copy')
        }
      })
    })
  }
})()
