<?php

namespace app\models;


use app\helpers\SQL;
use Yii;
use yii\db\ActiveRecord;
use yii\db\mssql\PDO;
use yii\db\Query;

/**
 * Class Transfer
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property integer $amount
 * @property string $type
 * @property string $status
 * @property integer $node
 * @property string $text
 * @property string $wallet
 * @property string $txid
 * @property string $ip
 * @property string $created
 * @property string $time
 * @package app\models
 */
class Transfer extends ActiveRecord
{
    public static $systems = [
        'perfect' => 'Perfect Money',
        'payeer' => 'Payeer',
        'advcash' => 'AdvCash',
    ];

    public static $currencies = [
        'perfect' => 'EUR',
        'advcash' => 'EUR',
        'ethereum' => 'EUR',
    ];

    public static $types = ['buy', 'payment', 'withdraw', 'accrue', 'bonus'];

    public function rules() {
        return [
            ['from', 'integer'],
            ['to', 'integer'],
            ['amount', 'integer'],
            ['type', 'string'],
            ['status', 'string'],
            ['node', 'integer'],
            ['text', 'string'],
            ['wallet', 'string'],
            ['txid', 'string'],
            ['ip', 'string'],
            ['created', 'datetime'],
            ['time', 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'status' => Yii::t('app', 'Operation'),
            'created' => Yii::t('app', 'Time'),
            'amount' => Yii::t('app', 'Amount'),
            'type' => Yii::t('app', 'Type'),
            'text' => Yii::t('app', 'Comment'),
            'user' => Yii::t('app', 'User'),
        ];
    }

    public static function keyBy($rows, $key = 'currency') {
        $result = [];
        foreach ($rows as $row) {
            $result[$row[$key]] = $row['amount'];
        }
        return $result;
    }

    public static function getFinance($user) {
        $rows = (new Query())->from('finance')
            ->where(['user' => $user])
            ->select(['type', 'currency', 'amount'])
            ->all();
        $finance = [];
        $additional = ['balance'];
//        $additional = ['balance', 'turnover', 'daily'];
        foreach (array_merge($additional, static::$types) as $type) {
            $finance[$type] = [];
            foreach (array_values(static::$currencies) as $currency) {
                $finance[$type][$currency] = 0;
            }
        }
        foreach ($rows as $row) {
            $finance[$row['type']][$row['currency']] = $row['amount'];
        }
        foreach ($additional as $key) {
            $amount = SQL::queryAll("SELECT currency, amount FROM $key WHERE \"user\" = :user",
                [':user' => $user], PDO::FETCH_KEY_PAIR);
            if ($amount) {
                $finance[$key] = $amount;
            }
        }
        return $finance;
    }

    public static function getExchange() {
        return ['EUR' => ['EUR' => 100]];
    }

    public function getDescription() {
        if ($this->text) {
            if ($this->vars) {
                return Yii::t("app", $this->text, json_decode($this->vars, JSON_OBJECT_AS_ARRAY));
            }
            return Yii::t("app", $this->text);
        }
        switch ($this->type) {
            case 'payment':
                return Yii::t('app', 'Payment from {wallet}', ['wallet' => $this->wallet]);
            case 'withdraw':
                return Yii::t('app', 'Withdrawal to {wallet}', ['wallet' => $this->wallet]);
            case 'support':
                return Yii::t('app', 'Administrative');
            default:
                return $this->type;
        }
    }

    public static function getTicker() {
        $texts = [];
        foreach (Transfer::find()->limit(10)->all() as $t) {
            $texts[] = $t->getDescription();
        }
        return $texts;
    }
}
