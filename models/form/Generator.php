<?php

namespace app\models\form;


use app\models\Deposit;
use app\models\Program;
use app\models\Transfer;
use app\models\User;
use Faker\Factory;
use yii\base\Model;

class Generator extends Model
{
    public $sponsor;
    public $count;
    public $program;
    public $eur;

    public function rules() {
        return [
            [['sponsor', 'count'], 'required'],
            ['eur', 'number'],
            ['program', 'integer'],
        ];
    }

    public function attributeLabels() {
        return [
            'eur' => 'Баланс',
            'program' => 'Площадка',
        ];
    }

    public function generate() {
        $factory = Factory::create();
        $parent = User::findOne(['nick' => $this->sponsor]);
        $results = [];
        for ($i = 0; $i < $this->count; $i++) {
            $user = new User([
                'nick' => $factory->userName,
                'email' => $factory->email,
                'forename' => $factory->firstName,
                'surname' => $factory->lastName,
                'parent' => $parent->id,
                'type' => 'native',
                'secret' => '$2a$10$uIexSvSQYugUCUMT/eIE7ezjNEAxUEXKq/eGzjM8OL1LhtPSpEjwe'
            ]);
            if ($user->save(false)) {
                if ($this->eur > 0) {
                    $amount = $this->eur * 100;
                    $transfer = new Transfer([
                        'to' => $user->id,
                        'type' => 'payment',
                        'status' => 'success',
                        'amount' => $amount,
                        'currency' => 'EUR'
                    ]);
                    if ($transfer->save(false)) {
                        if ($this->program > 0) {
                            $result = Program::buy($this->program, $user->id);
                            if ($result <= 0) {
                                $results[$user->nick]['EUR'] = [
                                    'status' => $result,
                                    'amount' => $amount
                                ];
                            }
                        }
                    } else {
                        return $transfer->getErrors();
                    }
                }
            } else {
                $errors = $user->getErrors();
                foreach ($errors as $name => $error) {
                    $errors[$name] = [
                        'value' => $user->$name,
                        'message' => $error
                    ];
                }
            }
        }
        return $results;
    }
}
