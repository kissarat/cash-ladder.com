<?php

namespace app\models\form;


use Yii;
use yii\base\Model;

class PasswordReset extends Model {
    public $password;
    public $repeat;

    public function rules() {
        return [
            [['password', 'repeat'], 'required'],
            ['password', 'compare', 'compareAttribute' => 'repeat']
        ];
    }

    public function attributeLabels() {
        return [
            'password' => Yii::t('app', 'Password'),
            'repeat' => Yii::t('app', 'Repeat Password')
        ];
    }
}
