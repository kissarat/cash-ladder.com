<?php

namespace app\models\form;

use Yii;

/**
 * Class Withdraw
 * @property string wallet
 * @property string text
 * @package app\models\form
 */
class Withdraw extends Invoice {
    public $wallet;
    public $text;

    public static $minimal = [
        'BTC' => 1000,
        'ETH' => 1000,
        'USD' => 400,
        'EUR' => 400,
    ];

    public function rules() {
        $rules = parent::rules();
        $rules[] = ['wallet', 'string'];
        $rules[] = ['wallet', 'required'];
        $rules[] = ['text', 'string'];
        return $rules;
    }

    public function attributeLabels() {
        $labels = parent::attributeLabels();
        $labels['text'] = Yii::t('app', 'Comment');
        return $labels;
    }
}
