<?php

namespace app\models\form;


use Yii;
use yii\base\Model;

/**
 * Class Password
 * @package app\models
 */
class Password extends Model {
    public $user_name;
    public $current;
    public $new;
    public $repeat;

    public function rules() {
        return [
            [['current', 'new', 'repeat'], 'required'],
            ['repeat', 'compare', 'compareAttribute' => 'new']
        ];
    }

    public function scenarios() {
        return [
            'default' => ['current', 'new', 'repeat'],
            'reset' => ['new', 'repeat'],
        ];
    }

    public function attributeLabels() {
        return [
            'current' => Yii::t('app', 'Current password'),
            'new' => Yii::t('app', 'New password'),
            'repeat' => Yii::t('app', 'Repeat password'),
        ];
    }
}
