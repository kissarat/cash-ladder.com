<?php

namespace app\models\form;

use Yii;
use yii\base\Model;

class Invoice extends Model {
    public $system;
    public $amount;
    public $wallet;

    public function rules() {
        return [
            ['amount', 'required'],
            ['system', 'string'],
            ['amount', 'number']
        ];
    }

    public function attributeLabels() {
        return [
            'system' => Yii::t('app', 'Payment system')
        ];
    }
}
