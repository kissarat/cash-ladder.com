<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

class Referral extends ActiveRecord {
    public function rules() {
        return [
            [['children', 'level'], 'integer'],
            [['nick', 'surname', 'forename', 'skype', 'email', 'sponsor'], 'string'],
        ];
    }

    public function attributeLabels() {
        return [
            'nick' => Yii::t('app', 'Username'),
            'forename' => Yii::t('app', 'First Name'),
            'surname' => Yii::t('app', 'Last Name'),
            'children' => Yii::t('app', 'Children'),
            'sponsor' => Yii::t('app', 'Sponsor'),
        ];
    }

    public static function tableName() {
        return 'referral_sponsor';
    }
}
