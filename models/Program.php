<?php

namespace app\models;


use app\helpers\SQL;
use app\helpers\Utils;
use yii\db\ActiveRecord;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property float $accrue
 * @property float $min
 * @property float $max
 * @property string $currency
 * @package app\models
 */
class Program extends ActiveRecord
{
    public static function transfer($params = []) {
        $params['status'] = 'success';
        $params['created'] = Utils::timestamp();
//        if (!empty($params['vars'])) {
//            $params['vars'] = json_encode($params['vars']);
//        }
        return (new Transfer($params))->insert(false);
    }

    /**
     * @param $program_id
     * @param $user_id
     * @param bool $straight
     * @param int $invest_id
     * @return int
     * @throws \Error
     */
    public static function buy(int $program_id, $user_id, $straight = false, $invest_id = null) : int {
        $user = null;
        $program = SQL::queryByKey('program_refund', $program_id);
        if (!$program) {
            return -404;
        }

        if ($straight && $user_id > 0) {
            $balance = SQL::queryByKey('balance', $user_id, 'user');
            if (!$balance || $balance['amount'] < $program['price']) {
                return -402;
            }
        }

        $parent_id = SQL::scalar('SELECT id FROM need WHERE "program" = :program_id LIMIT 1', [
            ':program_id' => $program_id
        ]);
        assert($parent_id > 0, 'Parent id not found');
        $parent = SQL::queryByKey('node', $parent_id);
        assert($parent, 'Parent not found');

        $node = new Node([
            'parent' => $parent_id,
            'program' => $program_id,
            'user' => $user_id,
            'reinvest' => $invest_id,
            'created' => Utils::timestamp()
        ]);
        $node->insert(false);
        assert($node->id > 0, 'ID of node is not positive');
        if ($user_id > 0) {
            if ($straight) {
                static::transfer([
                    'type' => 'buy',
                    'from' => $user_id,
                    'amount' => $program['price'],
                    'node' => $node->id
                ]);
            }

            $user = SQL::queryByKey('user', $user_id);
            assert($user && $user['id'] > 0, 'User not found');
            if ($user['parent'] > 0) {
                $sponsor_node = SQL::queryOne('SELECT * FROM node
                    WHERE "user" = :user_id AND "program" = :program_id LIMIT 1', [
                    ':user_id' => $user_id,
                    ':program_id' => $program_id
                ]);

                if ($sponsor_node) {
                    assert($sponsor_node['id'] > 0, 'Invalid sponsor id not found');
                    $sponsor = SQL::queryByKey('user', $sponsor_node['user']);
                    static::transfer([
                        'type' => 'bonus',
                        'to' => $user['parent'],
                        'amount' => $program['referral'],
                        'node' => $sponsor_node['id'],
                        'text' => 'Bonus from {from_nick}',
                        'vars' => json_encode([
                            'from' => $user['id'],
                            'from_nick' => $sponsor['nick']
                        ])
                    ]);
                }
            }
        }

        $closed = SQL::scalar('SELECT "count" FROM child_count WHERE id = :id', [':id' => $parent_id]) >= 4;

//        echo $parent['expires'] . '  ' . strtotime($parent['expires']);
        $expires = strtotime($parent['expires']);
        assert($expires > 0, 'Invalid expires');
        if ($parent['user'] > 0 && $expires < time()) {
            if ($program_id >= 6) {
                static::transfer([
                    'type' => 'refund',
                    'to' => $parent['user'],
                    'amount' => $program['price'] / 4,
                    'node' => $parent['id']
                ]);
            } elseif ($closed) {
                static::transfer([
                    'type' => 'refund',
                    'to' => $parent['user'],
                    'amount' => $program['price'],
                    'node' => $parent['id']
                ]);
            }
        }

        if ($closed) {
            for ($i = 0; $i < abs($program['reinvest']); $i++) {
                static::buy(1,
                    $program['reinvest'] > 0 ? $parent['user'] : null,
                    false, $node->id);
            }

            if ($user_id > 0 && $program['invest'] > 0) {
                return static::buy($program['invest'], $parent['user'],
                    false, $node->id);
            }
        }

        if ($straight) {
            SQL::execute("UPDATE node SET expires = CURRENT_TIMESTAMP + INTERVAL '1 week'
              WHERE \"user\" = :user AND expires >= CURRENT_TIMESTAMP", [
                ':user' => $user_id
            ]);
        }

        return $node->id;
    }
}
