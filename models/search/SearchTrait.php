<?php

namespace app\models\search;

use Yii;
use yii\data\ActiveDataProvider;

trait SearchTrait {
    public function formName() {
        return 's';
    }

    public function search($params, $options = []) {
        $query = $this->query();

        $options['query'] = $query;
        $provider = new ActiveDataProvider($options);

        if (!($this->load($params) && $this->validate())) {
            return $provider;
        }

        foreach ($this->strings as $key) {
            $query->andFilterWhere(['like', $key, $this->$key]);
        }

        foreach ($this->numbers as $key) {
            $query->andFilterWhere([$key => $this->$key]);
        }

        return $provider;
    }
}
