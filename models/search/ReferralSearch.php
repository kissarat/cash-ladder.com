<?php

namespace app\models\search;


use app\models\Referral;
use Yii;

class ReferralSearch extends Referral {
    use SearchTrait;

    public $strings = ['nick', 'surname', 'forename', 'skype', 'email', 'sponsor'];
    public $numbers = ['children', 'level'];

    public function query() {
        return static::find()
            ->select('id, created, eur, level, nick, email, skype, surname, forename, sponsor, children')
            ->where(['root' => Yii::$app->user->identity->user, 'level' => 1]);
//            ->andWhere(['>', 'level', 0])
//            ->andWhere(['>', 'level', 0])
//            ->andWhere(['<=', 'level', 3]);
    }
}
