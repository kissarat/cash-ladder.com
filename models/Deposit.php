<?php

namespace app\models;


use app\helpers\SQL;
use app\models\traits\DepositTrait;
use yii\db\ActiveRecord;

/**
 * Class Program
 * @property int $id
 * @property string $name
 * @property string $price
 * @property float $percent
 * @property float $min
 * @property float $max
 * @property string $currency
 * @package app\models
 */
class Deposit extends ActiveRecord {
    public $amount;
    private $_range;

    protected function getIntegerRange() {
        if (!$this->_range) {
            preg_match('|^.(\d+),(\d+).$|', $this->range, $this->_range);
        }
        return $this->_range;
    }

    public function getMin() {
        return $this->getIntegerRange()[1];
    }

    public function getMax() {
        return ($this->getIntegerRange()[2]) - 1;
    }

    public function rules() {
        return [
            ['amount', 'required'],
            ['id', 'integer'],
            ['name', 'string'],
            ['price', 'string'],
            ['amount', 'integer', 'integerOnly' => true, 'min' => $this->getMin(), 'max' => $this->getMax()]
        ];
    }

    public static function open(int $program, int $user, int $amount) {
        header("A: $program $user $amount");
        return SQL::scalar('SELECT open(:program, :user, :amount)', [
            ':program' => $program,
            ':user' => $user,
            ':amount' => $amount
        ]);
    }
}
