<?php

namespace app\models;


use app\helpers\Utils;
use yii\db\ActiveRecord;

class Email extends ActiveRecord {
    public static function send($to, $subject, $content) {
        $email = new Email([
            'to' => $to,
            'subject' => $subject,
            'content' => $content,
        ]);
        if ($email->save()) {
            Utils::mail($to, $subject, $content);
            $email->sent = Utils::timestamp();
            if ($email->save(false)) {
                return $email;
            }
        }
        return false;
    }
}
