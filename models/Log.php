<?php

namespace app\models;


use Yii;
use yii\db\ActiveRecord;

class Log extends ActiveRecord {
    public static function nanotime() {
        return (int) floor(microtime(true) * 10000) * 100000;
    }

    public static function log($entity, $action, $data = null, $user = null, $ip = null) {
        $id = static::nanotime();
        $u = Yii::$app->user;
        $log = new Log([
            'id' => $id,
            'entity' => $entity,
            'action' => $action,
            'data' => $data,
            'token' => isset($_COOKIE['PHPSESSID']) ? $_COOKIE['PHPSESSID'] : null,
            'user' => $user ?: ($u->getIsGuest() ? null : $u->identity->user),
            'ip' => $ip ?: Yii::$app->request->getUserIP(),
        ]);
        if ($log->save(false)) {
            return $log;
        }
        return false;
    }
}
