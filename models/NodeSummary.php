<?php

namespace app\models;


use Yii;

class NodeSummary extends Node
{
    public function attributeLabels() {
        return array_replace(parent::attributeLabels(), [
            'created' => Yii::t('app', 'Opening'),
            'name' => Yii::t('app', 'Program')
        ]);
    }
}
