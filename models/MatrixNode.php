<?php

namespace app\models;


class MatrixNode extends Node
{
//    public function rules() {
//        $rules = parent::rules();
//        $rules[] = ['count', 'integer'];
//        return $rules;
//    }

    public function attributeLabels() {
        $labels =  parent::attributeLabels();
        $labels['program'] = 'Ступень';
        $labels['count'] = 'Статистика';
        return $labels;
    }

    public static function tableName() {
        return 'matrix_node';
    }
}