<?php

namespace app\assets;


use yii\web\AssetBundle;

class LayoutAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css',
        '/css/main.css'
    ];
    public $js = [
        '//ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js',
        '//cdnjs.cloudflare.com/ajax/libs/vue/2.4.4/vue.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/es6-shim/0.35.3/es6-shim.min.js',
        '/js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public function init() {
        if (YII_DEBUG) {
            $this->js[] = '/js/inspector.js';
            $this->css[] = '/css/inspector.css';
        }
    }
}
