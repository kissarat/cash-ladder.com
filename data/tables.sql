CREATE TYPE "TOKEN_TYPE" AS ENUM (
  'server',
  'browser',
  'app',
  'code'
);

CREATE TYPE "USER_TYPE" AS ENUM ('new', 'native', 'leader', 'special');

CREATE TABLE "user" (
  id         SERIAL PRIMARY KEY                                  NOT NULL,
  nick       CHARACTER VARYING(24)                               NOT NULL,
  email      CHARACTER VARYING(48)                               NOT NULL,
  type       "USER_TYPE" DEFAULT 'new' :: "USER_TYPE"            NOT NULL,
  parent     INTEGER,
  secret     CHARACTER(60),
  admin      BOOLEAN DEFAULT FALSE                               NOT NULL,
  surname    CHARACTER VARYING(48),
  forename   CHARACTER VARYING(48),
  avatar     CHARACTER VARYING(192),
  skype      CHARACTER VARYING(32),
  phone      CHARACTER VARYING(32),
  country    CHARACTER VARYING(64),
  settlement CHARACTER VARYING(128),
  address    CHARACTER VARYING(128),
  perfect    CHARACTER VARYING(10),
  blockio    CHARACTER VARYING(40),
  ethereum   CHARACTER VARYING(42),
  geo        BOOLEAN                                             NOT NULL DEFAULT FALSE,
  created    TIMESTAMP(0) WITHOUT TIME ZONE                               DEFAULT now(),
  "time"     TIMESTAMP(0) WITHOUT TIME ZONE
);

CREATE TABLE token (
  id        CHARACTER VARYING(192)                                                                      NOT NULL,
  "user"    INTEGER,
  type      "TOKEN_TYPE"                                                                                NOT NULL DEFAULT 'browser',
  expires   TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT '2030-01-01 00:00:00' :: TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  data      JSON,
  ip        INET,
  handshake TIMESTAMP,
  name      CHARACTER VARYING(240),
  created   TIMESTAMP(0) WITHOUT TIME ZONE                                                                       DEFAULT now(),
  "time"    TIMESTAMP(0) WITHOUT TIME ZONE
);

CREATE TYPE "ARTICLE_TYPE" AS ENUM ('cat', 'page', 'product', 'ticket', 'note', 'article');

CREATE TABLE article (
  id       SERIAL PRIMARY KEY NOT NULL,
  path     VARCHAR(96) UNIQUE,
  "type"   "ARTICLE_TYPE"     NOT NULL DEFAULT 'page',
  price    INT,
  name     VARCHAR(96)        NOT NULL,
  short    VARCHAR(512),
  time     TIMESTAMP,
  created  TIMESTAMP          NOT NULL DEFAULT CURRENT_TIMESTAMP,
  text     VARCHAR(48000),
  image    VARCHAR(256),
  "user"   INT REFERENCES "user" (id),
  priority SMALLINT           NOT NULL DEFAULT 0,
  active   BOOLEAN            NOT NULL DEFAULT TRUE
);

CREATE TABLE relation (
  "from" INT NOT NULL REFERENCES article (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"   INT NOT NULL REFERENCES article (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  UNIQUE ("from", "to")
);

INSERT INTO article (path, name) VALUES
  ('faq', 'FAQ'),
  ('marketing', 'Маркетинг'),
  ('contacts', 'Контакты');

CREATE TABLE session (
  id     CHAR(40) NOT NULL PRIMARY KEY,
  expire INTEGER,
  data   BYTEA
);

CREATE TABLE cache (
  id     CHAR(128) NOT NULL PRIMARY KEY,
  expire INT,
  data   BYTEA
);

CREATE TABLE "visit" (
  "id"      SERIAL PRIMARY KEY,
  "token"   VARCHAR(240),
  "url"     VARCHAR(240) NOT NULL,
  "spend"   INT,
  ip        INET,
  agent     VARCHAR(240),
  "created" TIMESTAMP DEFAULT current_timestamp
);

CREATE TYPE "TRANSFER_TYPE" AS ENUM ('internal', 'payment', 'withdraw', 'accrue', 'buy', 'support',
  'write-off', 'bonus', 'refund', 'commission');
CREATE TYPE "TRANSFER_STATUS" AS ENUM ('created', 'success', 'fail', 'canceled', 'pending', 'virtual');
CREATE TYPE "TRANSFER_SYSTEM" AS ENUM ('perfect', 'advcash', 'payeer');
-- CREATE TYPE CURRENCY AS ENUM ('USD', 'EUR', 'GBP', 'JPY', 'CNY', 'RUB', 'PLN', 'UAH', 'KZT', 'BTC', 'ETH');
CREATE TYPE CURRENCY AS ENUM ('EUR');

CREATE TABLE transfer (
  id       SERIAL PRIMARY KEY,
  "from"   INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  "to"     INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  amount   INT8              NOT NULL,
  "type"   "TRANSFER_TYPE"   NOT NULL,
  "status" "TRANSFER_STATUS" NOT NULL,
  node     INT,
  text     VARCHAR(192),
  vars     JSON,
  wallet   VARCHAR(64),
  txid     VARCHAR(64),
  system   "TRANSFER_SYSTEM",
  currency CURRENCY          NOT NULL        DEFAULT 'EUR',
  ip       INET,
  created  TIMESTAMP         NOT NULL        DEFAULT CURRENT_TIMESTAMP,
  time     TIMESTAMP,
  visible  BOOLEAN           NOT NULL        DEFAULT TRUE
);

CREATE TABLE program (
  id        SMALLINT PRIMARY KEY,
  price     INT       NOT NULL,
  referral  INT       NOT NULL DEFAULT 0,
  reinvest  INT       NOT NULL DEFAULT 0,
  invest    SMALLINT REFERENCES program (id),
  pillow    INT       NOT NULL DEFAULT 0,
  changed   TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  start     SMALLINT  NOT NULL DEFAULT 1,
  direction SMALLINT  NOT NULL DEFAULT 0
);

INSERT INTO program (id, price, referral, invest, reinvest, pillow) VALUES
  (6, 30000, 750, NULL, -40, 3000),
  (5, 12000, 300, 6, 0, 4800),
  (4, 4100, 150, 5, -5, 0),
  (3, 1500, 100, 4, 0, 0),
  (2, 500, 50, 3, -1, 0),
  (1, 300, 25, 2, 1, 0);

CREATE TABLE deposit (
  id       SMALLINT PRIMARY KEY,
  name     VARCHAR(32) NOT NULL,
  range    INT8RANGE   NOT NULL,
  quantity SMALLINT    NOT NULL,
  accrue   FLOAT4      NOT NULL
);

INSERT INTO deposit (id, name, range, quantity, accrue) VALUES
  (11, 'Silver','[1000,100000000)', 180, 0.01),
  (12, 'Gold','[50000,100000000)', 270, 0.012),
  (13, 'Platinum','[100000,100000000]', 360, 0.014);

CREATE TABLE node (
  id       SERIAL PRIMARY KEY,
  "user"   INT REFERENCES "user" (id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  program  INT       NOT NULL,
  reinvest INT REFERENCES node (id),
  parent   INT REFERENCES node (id),
  active   BOOLEAN   NOT NULL DEFAULT TRUE,
  created  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  expires  TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP + INTERVAL '1 week',
  time     TIMESTAMP
);

CREATE INDEX node_program
  ON node (program);
CREATE INDEX node_parent
  ON node (parent);

ALTER SEQUENCE node_id_seq RESTART 51;

CREATE TYPE REQUEST_METHOD AS ENUM ('OPTIONS', 'GET', 'HEAD', 'POST', 'PUT', 'PATCH', 'DELETE', 'TRACE', 'CONNECT');

CREATE TABLE request (
  id     BIGSERIAL PRIMARY KEY,
  time   TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP,
  ip     INET,
  url    VARCHAR(4096)  NOT NULL,
  method REQUEST_METHOD NOT NULL,
  type   VARCHAR(64),
  agent  VARCHAR(512),
  "data" TEXT,
  guid   CHAR(32),
  token  VARCHAR(240),
  "user" INT
);

CREATE TABLE log (
  id     BIGINT PRIMARY KEY,
  entity VARCHAR(16) NOT NULL,
  action VARCHAR(32) NOT NULL,
  ip     INET,
  "user" INT REFERENCES "user" (id)
  ON DELETE CASCADE ON UPDATE CASCADE,
  token  CHARACTER VARYING(192),
  data   JSON
);

CREATE TABLE coordinate (
  id                SERIAL PRIMARY KEY,
  time              TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  latitude          FLOAT8    NOT NULL,
  longitude         FLOAT8    NOT NULL,
  altitude          FLOAT8,
  accuracy          SMALLINT,
  altitude_accuracy SMALLINT,
  heading           FLOAT8,
  speed             FLOAT8
);

CREATE TABLE wallet (
  id      VARCHAR(64) PRIMARY KEY,
  system  "TRANSFER_SYSTEM" NOT NULL,
  amount  INT8              NOT NULL,
  expires TIMESTAMP,
  time    TIMESTAMP         NOT NULL,
  created TIMESTAMP         NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TYPE NODE_STATUS AS ENUM ('reserved', 'opened', 'closed');

CREATE TABLE email (
  id      BIGSERIAL PRIMARY KEY,
  "to"    VARCHAR(192),
  subject VARCHAR(192),
  content TEXT,
  created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  sent    TIMESTAMP
);

CREATE TABLE exchange (
  "from" CURRENCY  NOT NULL,
  "to"   CURRENCY  NOT NULL,
  "rate" FLOAT     NOT NULL DEFAULT 1 CHECK ("rate" > 0),
  time   TIMESTAMP NOT NULL DEFAULT '1970-01-01 00:00:00',
  UNIQUE ("from", "to")
);

INSERT INTO exchange ("from", "to", rate) VALUES ('EUR', 'EUR', 100);

CREATE TABLE reward (
  level   SMALLINT PRIMARY KEY,
  percent FLOAT NOT NULL
);

INSERT INTO reward VALUES
  (1, 0.08),
  (2, 0.03),
  (3, 0.015),
  (4, 0.01),
  (5, 0.01),
  (6, 0.005);

CREATE TABLE "trace" (
  "id"       BIGSERIAL NOT NULL PRIMARY KEY,
  "level"    INTEGER,
  "category" VARCHAR(255),
  "log_time" DOUBLE PRECISION,
  "prefix"   TEXT,
  "message"  TEXT
);

CREATE INDEX "idx_log_level"
  ON "trace" ("level");
CREATE INDEX "idx_log_category"
  ON "trace" ("category");
