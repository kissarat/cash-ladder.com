<?php
$vars = [];
$filename = null;

foreach ($argv as $param) {
    $param = explode('=', $param);
    if (count($param) > 1) {
        $vars[$param[0]] = $param[1];
    }
    else {
        $filename = $param[0];
    }
}

$string = file_get_contents($filename);

foreach ($vars as $name => $value) {
    $string = str_ireplace($name, $value, $string);
}

echo $string;
