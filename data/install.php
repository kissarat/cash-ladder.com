<?php

$config = require __DIR__ . '/../config/common.php';
$dsn = $config['components']['db']['dsn'];
$username = $config['components']['db']['username'];
$password = $config['components']['db']['password'];
$dbname = null;
preg_match('/dbname=([^;]+)/', $dsn, $dbname);

if ($dbname) {
    $dbname = $dbname[1];
}
else {
    echo 'Database name undefined';
    exit();
}

$postgres = new PDO('pgsql:host=localhost;dbname=postgres');
$postgres->exec("DROP DATABASE \"$dbname\" IF EXISTS");
$postgres->exec("DROP USER \"$username\" IF EXISTS");
$postgres->exec("CREATE USER \"$username\" PASSWORD '$password'");
$postgres->exec("CREATE DATABASE \"$dbname\" OWNER \"$username\"");

$pdo = new PDO($dsn, $username, $password);
$pdo->exec(file_get_contents(__DIR__ . '/tables.sql'));
$pdo->exec(file_get_contents(__DIR__ . '/views.sql'));
//$pdo->exec(file_get_contents(__DIR__ . '/procedures.sql'));
