CREATE SCHEMA meta;
CREATE VIEW meta.reference AS
  SELECT
    kcu.constraint_name AS NAME,
    kcu.table_name      AS "from",
    kcu.column_name     AS "from_field",
    ccu.table_name      AS "to",
    ccu.column_name     AS "to_field",
    rc.update_rule,
    rc.delete_rule
  FROM information_schema.key_column_usage kcu
    JOIN information_schema.constraint_column_usage ccu ON kcu.constraint_name = ccu.constraint_name
    JOIN information_schema.referential_constraints rc ON kcu.constraint_name = rc.constraint_name;

CREATE VIEW meta.attribute AS
  SELECT
    c.relname                                                        AS name,
    json_object_agg(a.attname, format_type(a.atttypid, a.atttypmod)) AS attributes
  FROM pg_class c
    JOIN pg_attribute a ON a.attrelid = c.oid
    JOIN pg_namespace n ON c.relnamespace = n.oid
  WHERE nspname = 'public' AND (relkind = 'r' OR relkind = 'v') AND a.attstattarget <> 0
  GROUP BY c.relname
  ORDER BY relname;

CREATE VIEW token_user AS
  SELECT
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.parent,
    row_to_json(u.*) AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id;

CREATE VIEW "identity" AS
  SELECT
    t.created,
    t.id,
    t.ip,
    t.time,
    t.type,
    t.expires,
    u.email,
    u.nick,
    u.admin,
    u.avatar,
    u.surname,
    u.forename,
    u.parent,
    u.created AS user_created,
    u.id      AS "user"
  FROM token t
    LEFT JOIN "user" u ON t.user = u.id;

CREATE VIEW user_about AS
  SELECT
    id,
    type,
    nick,
    forename,
    surname,
    avatar,
    email,
    skype,
    phone,
    country,
    settlement,
    address,
    admin
  FROM "user";

CREATE VIEW transfer_user AS
  SELECT
    tr.id,
    tr.amount,
    tr.type,
    tr.status,
    tr.wallet,
    tr.ip,
    tr.created,
    tr.time,
    tr."from",
    tr."to",
    tr.currency,
    tr.txid,
    f.nick AS from_nick,
    t.nick AS to_nick
  FROM transfer tr
    LEFT JOIN "user" f ON f.id = tr."from"
    LEFT JOIN "user" t ON t.id = tr."to";

CREATE VIEW user_transfer AS
  SELECT
    *,
    CASE WHEN type IN ('buy', 'withdraw')
      THEN "from"
    ELSE "to" END AS "user"
  FROM transfer;

CREATE VIEW balance AS
  WITH
      a AS (
      SELECT
        "to" AS "user",
        currency,
        amount
      FROM transfer it
      WHERE "to" IS NOT NULL AND it.status = 'success'
      UNION ALL
      SELECT
        "from" AS "user",
        currency,
        -amount
      FROM transfer ot
      WHERE "from" IS NOT NULL AND ot.status = 'success'
    ),
      b AS (
        SELECT
          a."user",
          currency,
          sum(amount) AS amount
        FROM a
        GROUP BY a."user", currency
    )
  SELECT
    u.id                  AS "user",
    currency,
    coalesce(b.amount, 0) AS amount
  FROM b
    RIGHT JOIN "user" u ON b."user" = u.id;

CREATE VIEW referral AS
  WITH RECURSIVE r(id, "nick", parent, email, root, level, forename, surname, skype, avatar, created) AS (
    SELECT
      id,
      nick,
      parent,
      email,
      id AS root,
      0  AS level,
      forename,
      surname,
      skype,
      avatar,
      created
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.parent,
      u.email,
      r.root,
      r.level + 1 AS level,
      u.forename,
      u.surname,
      u.skype,
      u.avatar,
      u.created
    FROM "user" u
      JOIN r ON u.parent = r.id
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    parent,
    email,
    root,
    level,
    forename,
    surname,
    skype,
    avatar,
    created
  FROM r
  ORDER BY root, level, id;

CREATE VIEW finance AS
  SELECT
    t."user",
    t.type,
    t.currency,
    sum(t.amount) AS amount
  FROM user_transfer t
  WHERE t.status = 'success'
  GROUP BY t."user", t.type, t.currency;

CREATE VIEW informer AS
  SELECT
    u.id,
    (SELECT count(*)
     FROM "visit" v
     WHERE v.url = '/ref/' || u.nick)                AS visit,
    (SELECT count(*)
     FROM "user" r
     WHERE r.parent = u.id)                          AS invited,
    (SELECT count(*)
     FROM "user"
     WHERE parent = u.id)                            AS referral,
    (SELECT count(*)
     FROM node n
       JOIN "user" c ON n."user" = c.id
     WHERE c.parent = u.id)                          AS node,
    extract(DAYS FROM CURRENT_TIMESTAMP - u.created) AS day,
    u.nick,
    u.type,
    u.surname,
    u.forename,
    u.email,
    u.avatar,
    u.skype,
    u.phone,
    u.country,
    u.settlement,
    u.address,
    to_json(p.*)                                     AS parent
  FROM "user" u
    LEFT JOIN user_about p ON u.parent = p.id;

CREATE VIEW admin_user AS
  SELECT
    u.id,
    u.nick,
    u.type,
    u.admin,
    u.surname,
    u.forename,
    u.skype,
    u.avatar,
    u.email,
    u.parent,
    s.nick                               AS sponsor,
    coalesce((SELECT count(*) :: INT
              FROM "user" c
              WHERE c.parent = u.id), 0) AS children,
    coalesce((SELECT b.amount
              FROM balance b
              WHERE b."user" = u.id), 0) AS eur
  FROM "user" u LEFT JOIN "user" s ON u.parent = s.id;

CREATE VIEW sponsor AS
  WITH RECURSIVE r(id, "nick", type, parent, root, level) AS (
    SELECT
      id,
      nick,
      type,
      parent,
      id AS root,
      0  AS level
    FROM "user"
    UNION
    SELECT
      u.id,
      u.nick,
      u.type,
      u.parent,
      r.root,
      r.level + 1 AS level
    FROM "user" u
      JOIN r ON u.id = r.parent
    WHERE r.level < 50
  )
  SELECT
    id,
    nick,
    type,
    parent,
    root,
    level
  FROM r
  ORDER BY root, level, id;

CREATE VIEW table_size AS
  WITH
      a AS (
        SELECT
          c.oid,
          nspname                               AS table_schema,
          relname                               AS "table_name",
          c.reltuples                           AS row_estimate,
          pg_total_relation_size(c.oid)         AS total_bytes,
          pg_indexes_size(c.oid)                AS index_bytes,
          pg_total_relation_size(reltoastrelid) AS toast_bytes
        FROM pg_class c
          LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
        WHERE relkind = 'r'
    ),
      b AS (
        SELECT
          *,
          total_bytes - index_bytes - COALESCE(toast_bytes, 0) AS table_bytes
        FROM a
    )
  SELECT
    "table_name",
    pg_size_pretty(total_bytes) AS total,
    pg_size_pretty(index_bytes) AS INDEX,
    pg_size_pretty(toast_bytes) AS toast,
    pg_size_pretty(table_bytes) AS "table",
    total_bytes
  FROM b
  WHERE table_schema = 'public';

CREATE VIEW top_referrals AS
  SELECT
    replace(url, '/ref/', '') AS nick,
    count
  FROM (SELECT
          url,
          count(*)
        FROM visit
        WHERE url LIKE '/ref/%'
        GROUP BY url) t
  WHERE count > 1
  ORDER BY count DESC;

CREATE VIEW consolidated_transfer AS
  SELECT
    type,
    status,
    amount
  FROM transfer t;

CREATE VIEW consolidated_balance AS
  SELECT amount
  FROM balance;

CREATE VIEW statistics AS
  SELECT
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'payment' AND status = 'success')                        AS payment,
    coalesce((SELECT sum(amount)
              FROM consolidated_transfer
              WHERE type = 'payment' AND status =
                                         'pending'), 0)                    AS pending,
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'withdraw' AND status =
                                 'success')                                AS withdraw,
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'accrue' AND status =
                               'success')                                  AS accrue,
    (SELECT sum(amount)
     FROM consolidated_transfer
     WHERE type = 'buy' AND status =
                            'success')                                     AS buy,
    (SELECT count(*)
     FROM "user")                                                          AS users,
    (SELECT count(*)
     FROM "user" ud
     WHERE ud.created > CURRENT_TIMESTAMP -
                        INTERVAL '1 day')                                  AS users_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP)               AS visitors,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS visitors_day,
    (SELECT count(*)
     FROM "token"
     WHERE type = 'browser' AND expires > CURRENT_TIMESTAMP
           AND handshake > CURRENT_TIMESTAMP -
                           INTERVAL '1 day')                               AS new_visitors_day,
    (SELECT count(*)
     FROM log
     WHERE entity = 'handshake' AND action = 'update'
           AND to_timestamp(id / (1000 * 1000 * 1000)) > CURRENT_TIMESTAMP -
                                                         INTERVAL '1 day') AS activities_day,
    (SELECT count(*)
     FROM visit
     WHERE url LIKE '/ref/%' AND visit.created > CURRENT_TIMESTAMP -
                                                 INTERVAL '1 day')         AS referral_day,
    (SELECT sum(amount)
     FROM consolidated_balance)                                            AS balance,
    NOW()                                                                  AS "time";

CREATE VIEW withdrawable AS
  SELECT
    t.id,
    (CASE WHEN b.amount - t.amount >= 0
      THEN 'success'
     ELSE 'canceled' END) AS will,
    t.amount,
    t.wallet,
    t."from",
    u.nick                AS "from_nick",
    b.amount              AS balance,
    t.txid,
    b.amount - t.amount   AS remain,
    t.ip,
    t.text,
    t.vars,
    t.system,
    t.created
  FROM transfer t
    JOIN balance b ON t."from" = b."user" AND t.currency = b.currency
    JOIN "user" u ON t."from" = u.id
  WHERE t.type = 'withdraw' AND t.status = 'created' AND t.wallet IS NOT NULL
        AND t.txid IS NULL AND t.node IS NULL AND t.amount > 0 AND t.ip IS NOT NULL;

CREATE OR REPLACE VIEW program_direction AS
  SELECT
    id,
    price,
    price * 4 - pillow

    - coalesce((SELECT price
                FROM program nx
                WHERE nx.id = p.invest), 0) AS refund,
    referral,
    reinvest,
    invest,
    pillow,
    changed,
    CASE WHEN CURRENT_TIMESTAMP < changed + INTERVAL '3 day'
      THEN start
    ELSE 1 END                              AS start,
    CASE WHEN CURRENT_TIMESTAMP < changed + INTERVAL '3 day'
      THEN direction
    ELSE 0 END                              AS direction
  FROM program p;

CREATE OR REPLACE VIEW program_refund AS
  SELECT
    id,
    price,
    price * 4
    - pillow
    - p.referral * 4
    - coalesce((SELECT abs(p.reinvest) * p1.price
                FROM program p1
                WHERE p1.id = 1), 0)
    - coalesce((SELECT price
                FROM program nx
                WHERE nx.id = p.invest), 0) AS refund,
    referral,
    reinvest,
    invest,
    pillow
  FROM program p;

CREATE OR REPLACE VIEW child_count AS
  SELECT
    n.id,
    count(c.id) AS "count"
  FROM node n
    JOIN node c ON n.id = c.parent
  GROUP BY n.id;

CREATE OR REPLACE VIEW matrix AS
  WITH RECURSIVE r(program, root, id, parent, "user", level) AS (
    SELECT
      program,
      id AS root,
      id,
      parent,
      "user",
      0  AS "level"
    FROM node
    WHERE program < 10
    UNION
    SELECT
      n.program,
      r.root,
      n.id,
      n.parent,
      n."user",
      r.level + 1 AS "level"
    FROM node n
      JOIN r ON r.id = n.parent
    WHERE r."level" < 50
  )
  SELECT
    r.*,
    u.nick,
    coalesce(c.count < 4, TRUE) AS is_active
  FROM r
    LEFT JOIN "user" u ON r."user" = u.id
    LEFT JOIN child_count c ON c.id = r.id;

CREATE OR REPLACE VIEW pyramid AS
  WITH RECURSIVE r(program, root, id, parent, "user", level) AS (
    SELECT
      program,
      id AS root,
      id,
      parent,
      "user",
      0  AS "level"
    FROM node
    WHERE id < 10
    UNION
    SELECT
      n.program,
      r.root,
      n.id,
      n.parent,
      n."user",
      r.level + 1 AS "level"
    FROM node n
      JOIN r ON r.id = n.parent
    WHERE r."level" < 50
  )
  SELECT r.*
  FROM r;

CREATE OR REPLACE VIEW free_node AS
  SELECT
    p.id,
    p.program,
    p.level,
    p.parent
  FROM pyramid p
    LEFT JOIN child_count c ON p.id = c.id
  WHERE p."user" IS NOT NULL AND (c.id IS NULL OR c.count < 4);

CREATE OR REPLACE VIEW free_level AS
  SELECT
    p.program,
    p.level,
    count(p.id) AS free
  FROM free_node p
  GROUP BY p.program, p.level;

CREATE OR REPLACE VIEW first_free_level AS
  SELECT
    program,
    min(level) AS level
  FROM free_node
  GROUP BY program;

CREATE OR REPLACE VIEW need_direction AS
  SELECT
    id AS program,
    f.level,
    free,
    start,
    direction
  FROM first_free_level f
    JOIN free_level l ON f.program = l.program AND f.level = l.level
    JOIN program_direction p ON f.program = p.id;

CREATE OR REPLACE VIEW need_number AS
  SELECT
    program,
    level,
    CASE start
    WHEN -1
      THEN free
    WHEN 0
      THEN least(greatest(floor(free / 2) + direction, 1), free)
    ELSE 1
    END AS number
  FROM need_direction a;

CREATE OR REPLACE VIEW free_node_number AS
  SELECT
    id,
    program,
    level,
    (row_number()
    OVER (
      PARTITION BY p.program, p.level
      ORDER BY p.parent, p.id ))
      AS number
  FROM free_node p;

CREATE OR REPLACE VIEW need AS
  SELECT
    n.program,
    p.id
  FROM need_number n
    LEFT JOIN free_node_number p ON n.program = p.program AND n.level = p.level AND n.number = p.number;

CREATE OR REPLACE VIEW node_deposit AS
  SELECT
    n.id,
    n."user",
    n.program,
    d.accrue,
    d.quantity,
    t.amount,
    n.created,
    t.created AS bought
  FROM node n
    JOIN deposit d ON n.program = d.id AND n.id > 10
    JOIN transfer t ON n.id = t."node" AND t.type = 'buy' AND t.status = 'success';

CREATE OR REPLACE VIEW plan AS
  WITH t AS (
      SELECT
        n.id                                              AS node,
        number,
        n.user,
        floor(n.amount * n.accrue + 0.0001) :: BIGINT     AS amount,
        (SELECT n.created + number * INTERVAL '1 minute') AS time,
        n.quantity,
        n.created,
        n.bought
      FROM node_deposit n CROSS JOIN generate_series(1, n.quantity) number
  )
  SELECT
    node,
    "number",
    'accrue' :: "TRANSFER_TYPE"  AS type,
    "user",
    amount,
    time,
    created,
    bought,
    'Accrue {number}/{quantity}' AS text,
    json_build_object(
        'number', "number",
        'quantity', quantity,
        'income', amount * "number"
    )                            AS vars
  FROM t;

CREATE OR REPLACE VIEW need_deposit AS
  SELECT
    p.node,
    'accrue' :: "TRANSFER_TYPE" AS type,
    p.user,
    p.amount,
    p.number,
    p.text,
    p.vars,
    p.created,
    p.bought
  FROM plan p
  WHERE p.time <= CURRENT_TIMESTAMP
        AND p.number >
            (SELECT count(*)
             FROM transfer t
             WHERE t.node = p.node AND type = 'accrue');

CREATE OR REPLACE VIEW node_summary AS
  WITH a AS (
      SELECT
        node,
        sum(amount) AS profit,
        count(*)    AS accrues
      FROM transfer
      WHERE type = 'accrue'
      GROUP BY node
  )
  SELECT
    n.id,
    n.program,
    n."user",
    b.amount,
    'EUR' :: CURRENCY                                                   AS currency,
    coalesce(a.profit, 0)                                               AS profit,
    coalesce(a.accrues, 0)                                              AS accrues,
    n.created,
    b.created                                                           AS bought,
    n.created + (p.quantity + 1) * INTERVAL '1 day'                     AS closing,
    CASE WHEN n.active
      THEN 'opened'
    ELSE 'closed' END                                                   AS status,
    n.created + (p.quantity + 1) * INTERVAL '1 day' < CURRENT_TIMESTAMP AS closable
  FROM node n
    JOIN deposit p ON n.program = p.id
    JOIN transfer b ON b.node = n.id AND b.type = 'buy'
    LEFT JOIN a ON a.node = n.id
  WHERE n.program > 10;

CREATE OR REPLACE VIEW user_node_summary AS
  SELECT
    n.*,
    u.nick
  FROM node_summary n
    JOIN "user" u ON n."user" = u.id;

CREATE OR REPLACE VIEW article_author AS
  SELECT
    a.*,
    u.nick,
    u.surname,
    u.forename,
    u.email,
    u.skype,
    u.avatar,
    u.admin
  FROM article a LEFT JOIN "user" u ON a."user" = u.id;

CREATE OR REPLACE VIEW matrix_node AS
  SELECT
    n.*,
    c.count
  FROM node n
    LEFT JOIN child_count c ON n.id = c.id
  WHERE program < 10;

CREATE OR REPLACE VIEW user_matrix_node AS
  SELECT
    n.*,
    u.nick
  FROM matrix_node n
    LEFT JOIN "user" u ON n."user" = u.id;

CREATE OR REPLACE VIEW node_user AS
  SELECT
    n.parent,
    n.id,
    u.nick
  FROM node n
    LEFT JOIN public."user" u ON n."user" = u.id;

CREATE OR REPLACE VIEW referral_sponsor AS
  SELECT
    r.*,
    u.nick                                                                     AS sponsor,
    coalesce((SELECT sum(amount)
              FROM transfer
              WHERE "from" = r.id AND type = 'bonus' AND currency = 'EUR'), 0) AS eur,
    (SELECT count(*)
     FROM "user" c
     WHERE c.parent = r.id)                                                    AS children
  FROM referral r
    JOIN "user" u ON r.parent = u.id
    JOIN reward w ON w.level = r.level;

CREATE OR REPLACE VIEW transfer_statistics AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW transfer_week AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 week' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW transfer_day AS
  SELECT
    currency,
    type,
    sum(amount) AS amount
  FROM transfer
  WHERE created > CURRENT_TIMESTAMP - INTERVAL '1 day' AND status = 'success'
  GROUP BY currency, type;

CREATE OR REPLACE VIEW all_balance AS
  SELECT
    currency,
    sum(amount) AS amount
  FROM balance
  WHERE currency IS NOT NULL
  GROUP BY currency;

CREATE OR REPLACE VIEW commission AS
  SELECT
    "from"      AS "user",
    sum(amount) AS amount
  FROM transfer
  WHERE type = 'commission'
  GROUP BY "from";

CREATE OR REPLACE VIEW need_commission AS
  WITH a AS (
      SELECT
        t."to"                           AS "user",
        ceil(sum(amount) / 10) :: BIGINT AS amount
      FROM transfer t
        JOIN node n
          ON t.node = n.id
      WHERE TYPE = 'bonus' AND PROGRAM < 10
      GROUP BY t."to"
  )
  SELECT
    a."user",
    a.amount - coalesce(c.amount, 0) as amount
  FROM a
    LEFT JOIN commission c ON c."user" = a."user";
