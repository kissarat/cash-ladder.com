CREATE OR REPLACE FUNCTION open(program_id INT, user_id INT, _amount BIGINT)
  RETURNS INT AS $$
DECLARE
  node_id  INT;
  _created TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
BEGIN
  IF (SELECT count(*) <= 0
      FROM deposit p
      WHERE id = program_id AND _amount <@ p.range)
  THEN
    RETURN -404;
  END IF;

  IF coalesce((SELECT amount - _amount < 0
               FROM balance
               WHERE "user" = user_id), TRUE)
  THEN
    RETURN -402;
  END IF;

  INSERT INTO node ("user", "program", created)
  VALUES (user_id, program_id, _created)
  RETURNING id
    INTO node_id;

  INSERT INTO transfer ("type", "status", "from", amount, node)
  VALUES (
    'buy',
    'success',
    user_id,
    _amount,
    node_id
  );

  RETURN node_id;
END
$$ LANGUAGE plpgsql VOLATILE;

CREATE OR REPLACE FUNCTION accrue()
  RETURNS VOID AS $$
DECLARE
  _time TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
BEGIN
  INSERT INTO transfer ("type", "status", "to", amount, created, node, "text", vars)
    SELECT
      "type",
      'success',
      "user",
      amount,
      _time,
      node,
      "text",
      vars
    FROM need_deposit
    ORDER BY created, number;
END
$$ LANGUAGE plpgsql VOLATILE;
