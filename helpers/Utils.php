<?php

namespace app\helpers;


use DateTime;
use DateTimeZone;
use Yii;
use yii\web\Response;

class Utils
{
    public static function getTimeZones() {
        static $timezones;
        if (!$timezones) {
            $timezones = [];
            foreach (DateTimeZone::listIdentifiers(DateTimeZone::ALL) as $tz) {
                $dtz = new DateTimeZone($tz);
                $timezones[$tz] = $dtz->getOffset(new DateTime());
            }
        }
        return $timezones;
    }

    public static function pick($array, $keys) {
        $result = [];
        foreach ($keys as $key) {
            if (!empty($array[$key])) {
                $result[$key] = $array[$key];
            }
        }
        return $result;
    }

    public static function timestamp($time = null) {
        if (!is_numeric($time)) {
            $time = $_SERVER['REQUEST_TIME'];
        }
        return date('Y-m-d H:i:s.u', $time);
    }

    public static function mail($to, $subject, $content) {
        return Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom([Yii::$app->params['email']])
            ->setSubject($subject)
            ->setTextBody($content)
            ->send();
    }

    public static function jsonp($result) {
        $callback = empty($_GET['callback']) ? '' : $_GET['callback'];
        Yii::$app->response->format = $callback ? Response::FORMAT_JSONP : Response::FORMAT_JSON;
        $data = [];
        if ($callback) {
            $data['callback'] = $callback;
            $data['data'] = ['result' => $result];
        } else {
            $data['result'] = $result;
            if (!Yii::$app->user->getIsGuest()) {
                $data['nick'] = Yii::$app->user->identity->nick;
            }
        }
        return $data;
    }

    public static function getLocale($locale) {
        $messages = require Yii::getAlias("@vendor/yiisoft/yii2/messages/$locale/yii.php");
        $filename = Yii::getAlias("@app/messages/$locale.json");
        if (file_exists($filename)) {
            $messages = array_replace($messages, json_decode(file_get_contents($filename), JSON_OBJECT_AS_ARRAY));
        }
        return $messages;
    }
}
