<?php

namespace app\helpers;


use PDO;
use Yii;

class SQL
{
    public static function scalar($sql, $params = []) {
        return Yii::$app->db->createCommand($sql, $params)->queryScalar();
    }

    public static function queryAll($sql, $params = [], $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->queryAll($fetch_mode);
    }

    public static function queryOne($sql, $params = [], $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand($sql, $params)->queryOne($fetch_mode);
    }

    public static function execute($sql, $params = []) {
        return Yii::$app->db->createCommand($sql, $params)->execute();
    }

    public static function queryByKey($table, $value, $key = 'id', $fetch_mode = PDO::FETCH_ASSOC) {
        return Yii::$app->db->createCommand("SELECT * FROM \"$table\" WHERE \"$key\" = :$key LIMIT 1",
            [":" . $key => $value])->queryOne($fetch_mode);
    }
}
