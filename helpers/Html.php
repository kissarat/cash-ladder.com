<?php

namespace app\helpers;

class Html extends \yii\bootstrap\Html {
    public static function money($values) {
        $out = [];
        foreach ($values as $currency => $value) {
            $out[] = Html::tag('span', $value, ['class' => strtolower($currency)]);
        }
        return Html::tag('span', implode(' ', $out), ['class' => 'money']);
    }

    public static function background($model, $property) {
        return isset($model->$property) && $model->$property ? "background-image: url('" . $model->$property . "')" : '';
    }

    public static function email($address) {
        return "<a href=\"mailto:$address\">$address</a>";
    }
}
