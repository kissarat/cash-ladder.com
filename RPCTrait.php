<?php

namespace app;


use yii\httpclient\Client;

/**
 * @property Client $client
 */
trait RPCTrait {
    public $client;
    public $host;

    public function init() {
        $this->client = new Client([
            'baseUrl' => $this->host,
            'requestConfig' => [
                'format' => Client::FORMAT_JSON
            ],
            'responseConfig' => [
                'format' => Client::FORMAT_JSON
            ],
        ]);
    }

    function __call($method, $params) {
        return $this->request($method, $params);
    }

    public function getRequestId() {
        return microtime(true) * 1000000 - 1507205042627000;
    }

    public function request($method, $params = []) {
        $options = [
            'jsonrpc' => '2.0',
            'id' => $this->getRequestId(),
            'method' => $method,
            'params' => $params,
        ];
//        header('RQ: ' . json_encode($options));
        $d = $this->client->post('', $options)
            ->send()
            ->getData();
        return isset($d['result']) ? $d['result'] : $d;
    }
}
