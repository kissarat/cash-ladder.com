<?php
use app\helpers\Html;
?>

<section id="content">
        <div class="container">
            <div class="row">
                <div class="col s12 m12 l12">
                    <h3 class="title"><?= $status . ' ' . $this->title ?></h3>
                </div>
            </div>
        </div>

    <div class="container">
        <div class="section">

            <p class="caption"><?= $error['message'] ?></p>

            <div class="row">
                <div class="col s12 m12 l12">
                    

    
   
    <?php
    if (Yii::$app->user->getIsGuest()) {
        echo Html::a('Login', ['user/login']);
    }
    else {
        // echo Html::a('Profile', ['user/profile', 'nick' => Yii::$app->user->identity->nick]);
        echo '<img src="/images/logo.png">';
    }
    ?>
</div>
