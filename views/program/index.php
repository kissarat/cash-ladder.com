<?php
use app\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/**
 * @var \yii\data\ActiveDataProvider $provider
 */

$columns = [[
    'attribute' => 'id',
    'label' => '№ в очереди'
]];
if (!$program_id) {
    $columns[] = 'program';
}
$columns[] = [
    'attribute' => 'count',
    'label' => 'Статистика',
    'value' => function ($model) {
        return $model->count . ' / 4';
    }
];
$columns[] = [
    'attribute' => 'created',
    'contentOptions' => function ($model) {
        return ['data-time' => strtotime($model->created)];
    }
]

?>
<article id="page-program-index">
    <section class="programs">
        <div>
            <?php foreach ($programs as $id => $price): ?>
                <?= Html::beginTag('a', ['href' => Url::to(['program/index',
                    'nick' => Yii::$app->user->identity->nick,
                    'id' => $id]),
                    'class' => $program_id ? 'current' : '']) ?>
                <img src="/images/<?= $id ?>.png">
                <div>
                    <div>Ступень <?= $id ?></div>
                    <div class="eur"><?= $price ?></div>
                    <?php
                    if ('GET' === $_SERVER['REQUEST_METHOD']):
                        $form = ActiveForm::begin() ?>
                        <?= Html::hiddenInput('id', $id) ?>
                        <button>Открыть</button>
                        <?php ActiveForm::end();
                    endif
                    ?>
                </div>
                <?= Html::endTag('a') ?>
            <?php endforeach; ?>
        </div>
    </section>
    <section>
        <?php if ($program_id > 0): ?>
            <div>
                <div>
                    <div>Ваш номер в этой очереди:</div>
                    <div>Ступень <span class="eur"><?= $programs[$program_id] ?></span></div>
                </div>
                <div>
                    <div>
                        <div></div>
                        <div></div>
                    </div>
                    <div>
                        <div>{{ children[0] || '-' }}</div>
                        <div>{{ children[1] || '-' }}</div>
                    </div>
                    <div>
                        <div>{{ children[2] || '-' }}</div>
                        <div>{{ children[3] || '-' }}</div>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <?= GridView::widget(['dataProvider' => $provider,
            'rowOptions' => function ($model) {
                return ['v-on:click' => "setProgramNode($model->id)"];
            },
            'columns' => $columns])
        ?>
    </section>
</article>
