<?php

use yii\helpers\Html;

function item($text, $url) {
    if (!Yii::$app->user->getIsGuest() && 'array' === gettype($url)) {
        $url['nick'] = Yii::$app->user->identity->nick;
    }
    return Html::a(Yii::t("app", $text), $url);
}

$this->beginBlock('menu');
?>
    <header v-bind:class="{menu: menu.active}" xmlns:v-on="http://www.w3.org/1999/xhtml"
            xmlns:v-bind="http://www.w3.org/1999/xhtml">
        <nav>
            <div class="open" v-on:click="menu.active = !menu.active">Меню</div>
            <div class="list">
                <?php if (Yii::$app->user->getIsGuest()): ?>
                    <?= item('Вход', ['user/login']) ?>
                    <?= item('Регистрация', ['user/signup']) ?>
                    <?= item('Забыли пароль?', ['user/request']) ?>
                    <?= item('Маркетинг', ['article/view', 'path' => 'marketing']) ?>
                    <?= item('FAQ', ['article/view', 'path' => 'faq']) ?>
                    <?= item('Новости', ['article/index']) ?>
                    <?= item('Контакт', ['article/view', 'path' => 'contacts']) ?>
                <?php else: ?>
                    <?= item('Выход', ['user/logout']) ?>
                    <?= item('Маркетинг', ['article/view', 'path' => 'marketing']) ?>
                    <?= item('FAQ', ['article/view', 'path' => 'faq']) ?>
                    <?= item('Новости', ['article/index']) ?>
                    <?= item('Контакт', ['article/view', 'path' => 'contacts']) ?>
                    <div class="split"></div>
                    <?= item('Кабинет', ['user/profile']) ?>
                    <?= item('Ступени', ['program/index']) ?>
                    <?= item('Депозиты', ['deposit/index']) ?>
                 <!--   <?= item('Статистика', ['home/construction']) ?> -->
                    <?= item('Финансы', ['transfer/index']) ?>
                    <?= item('Партнеры', ['structure/index', 'page' => 1]) ?>
                    <?= item('Профиль', ['settings/profile']) ?>
                <?php endif ?>
            </div>
        </nav>
        <div class="brand-text">
            CA$H LADDER
        </div>
        <img src="/images/skype.png"/>
    </header>
<?php
$this->endBlock();
