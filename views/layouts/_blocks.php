<?php

use app\assets\LayoutAsset;

/* @var $this \yii\web\View */

LayoutAsset::register($this);

$globalClasses = [Yii::$app->user->getIsGuest() ? 'guest' : 'registered'];
$nick = Yii::$app->user->getIsGuest() ? '' : Yii::$app->user->identity->nick;

if (empty($this->title)) {
    $this->title = 'Cash Ladder';
}
else {
    $this->title .= ' | Cash Ladder';
}

require '_menu.php';
require '_head.php';
