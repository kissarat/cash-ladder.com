<?php

use yii\helpers\Html;
use app\models\Transfer;

$this->beginBlock('head');

$ip = $_SERVER['REMOTE_ADDR'];

$config = [
    'ip' => $ip,
    'timezone' => date_default_timezone_get(),
    'systems' => Transfer::$systems,
    'currencies' => Transfer::$currencies,
    'token' => Yii::$app->user->getIsGuest() ? null :
        Yii::$app->user->identity->id,
    'guest' => Yii::$app->user->getIsGuest(),
//    'timezones' => Utils::getTimeZones(),
];

if (!Yii::$app->user->getIsGuest()) {
    foreach (['user', 'nick', 'parent', 'surname', 'forename', 'geo', 'user_created'] as $key) {
        $config[$key] = isset(Yii::$app->user->identity[$key])
            ? Yii::$app->user->identity[$key]
            : null;
    }
}

$config = json_encode($config, JSON_PRETTY_PRINT);
?>
    <meta charset="<?= Yii::$app->charset ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title><?= $this->title ?></title>

    <script>
      const config = (<?= $config ?>) || {guest: true};
    </script>
<?php
echo Html::csrfMetaTags();
$this->head();

$this->endBlock();
