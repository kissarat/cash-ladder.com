<?php
use app\Alert;

require '_blocks.php';

$globalClasses[] = 'outdoor';

if (empty($this->title)) {
    $this->title = Yii::t('app', 'Login');
}

$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->blocks['head']; ?>
</head>
<body class="<?= implode(' ', $globalClasses) ?>">
<?= $this->beginBody() ?>
<div id="app">
    <?= $this->blocks['menu'] ?>

    <main>
        <aside><?= Alert::widget() ?></aside>
        <div class="container">
            <?= $content ?>
        </div>
    </main>
</div>
<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
