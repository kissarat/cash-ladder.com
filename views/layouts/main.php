<?php
use app\Alert;

require '_blocks.php';

$globalClasses[] = 'indoor';

if (empty($this->title)) {
    $this->title = Yii::t('app', 'Back office');
}

$this->beginPage()
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <?= $this->blocks['head'] ?>
</head>
<body class="<?= implode(' ', $globalClasses) ?>">
<?= $this->beginBody() ?>
<div id="app">
    <?= $this->blocks['menu'] ?>

    <main v-cloak>
        <div class="cloak"></div>
        <aside><?= Alert::widget() ?></aside>
        <div class="container">
            <?= $content ?>
        </div>
    </main>
</div>

<footer class="center footer">
    <div class="w-container">
        <div class="footer-text">Copyright C$SH LADDER 2017</div>
    </div>
</footer>


<?= $this->endBody() ?>
</body>
</html>
<?= $this->endPage() ?>
