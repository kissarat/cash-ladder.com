<?php
use app\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>
<section id="page-node-index">
    <h1>Deposits</h1>
    <?= GridView::widget([
        'dataProvider' => $provider,
        'layout' => '{items}',
        'tableOptions' => ['class' => 'striped bordered'],
        'emptyText' => 'Nothing found',
        'rowOptions' => function ($model) {
            return ['class' => $model->status];
        },
        'columns' => [
            [
                'attribute' => 'created',
                'format' => 'datetime',
                'contentOptions' => function ($model) {
                    return ['data-date' => strtotime($model->created)];
                }
            ],
            [
                'attribute' => 'amount',
                'format' => 'html',
                'value' => function ($model) {
                    $sign = $model->user === Yii::$app->user->identity->user ? '+' : '-';
                    $currency = strtolower($model->currency);
                    return "<span class=\"sign\">$sign</span><span class=\"$currency\">$model->amount</span>";
                }
            ],
            'accrues',
            [
                'attribute' => 'profit',
                'format' => 'html',
                'value' => function ($model) {
                    $currency = strtolower($model->currency);
                    return "<span class=\"$currency\">$model->profit</span>";
                }
            ],
            'currency',

            [
                'attribute' => 'closing',
                'format' => 'datetime',
                'contentOptions' => function ($model) {
                    return ['data-date' => strtotime($model->closing)];
                }
            ],
        ]
    ]) ?>
</section>
