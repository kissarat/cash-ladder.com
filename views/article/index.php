<?php
use app\helpers\Html;

?>

<div id="page-article-index" class="section">
    <!--script>
        const articles = (<?= json_encode($models, JSON_UNESCAPED_UNICODE) ?>);
    </script-->
    <?php foreach ($models as $model): ?>
        <article class="article" itemscope itemtype="https://schema.org/NewsArticle">
            <div itemprop="identifier"><?= $model['id'] ?></div>
            <h2 itemprop="headline"><?= Html::a($model['name'], '/' . $model['path']) ?></h2>
            <div itemprop="description"><?= $model['name'] ?></div>
        </article>
    <?php endforeach ?>
</div>
