<?php
use \yarcode\payeer\RedirectForm;
use \yarcode\payeer\Merchant;
?>

<?= RedirectForm::widget([
    'merchant' => Yii::$app->get('payeer'),
    'invoiceId' => $id,
    'amount' => $amount,
    'description' => $description,
    'currency' => Merchant::CURRENCY_EUR
]);
?>

