<?php
use app\helpers\Html;
use yii\widgets\ActiveForm;
?>

<section>
    <?php

    $form = ActiveForm::begin();
    echo $form->field($model, 'sponsor');
    echo $form->field($model, 'count');
    echo $form->field($model, 'program');
    echo $form->field($model, 'eur');
    echo Html::submitButton('Generate');
    echo Html::a('Clear', ['generate/clear']);
    if (!empty($errors)) {
        echo Html::tag('pre', json_encode($errors, JSON_PRETTY_PRINT));
    }
    ActiveForm::end();
    ?>
</section>
