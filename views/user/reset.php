<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\RequestPasswordReset
 * @var $form ActiveForm
 */

$this->title = Yii::t('app', 'Password Recovery');

?>

<section id="page-reset">
    <h1><?= Yii::t('app', 'Reset password') ?></h1>
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => false,
    ]); ?>

    <?= $form->field($model, 'password')->passwordInput([
        'maxlength' => 100,
        'id' => 'password',
        'placeholder' => 'Пароль'
    ]) ?>
    <?= $form->field($model, 'repeat')->passwordInput([
        'maxlength' => 100,
        'id' => 'repeat',
        'placeholder' => 'Повторите пароль'
    ]) ?>

    <div>
        <?= Html::submitButton(Yii::t('app', 'Reset'),
            ['class' => 'button full-width']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
