<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<article id="page-signup" class="section">
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => false,
        'enableAjaxValidation' => false
    ]); ?>
    <h1><?= Yii::t('app', 'Create an account') ?></h1>

    <div class="pair">
        <?= $form->field($model, 'forename')->textInput([
            'maxlength' => 48,
            'id' => 'forename',
            'placeholder' => 'Имя'
        ]) ?>
        <?= $form->field($model, 'surname')->textInput([
            'maxlength' => 48,
            'id' => 'surname',
            'placeholder' => 'Фамилия'
        ]) ?>
    </div>
    <div class="pair">
        <?= $form->field($model, 'nick')->textInput([
            'maxlength' => 24,
            'id' => 'username',
            'placeholder' => 'Логин'
        ]) ?>
        <?= $form->field($model, 'sponsor')->textInput([
            'maxlength' => 24,
            'id' => 'sponsor',
            'placeholder' => 'Спонсор'
        ]) ?>
    </div>
    <div class="pair">
        <?= $form->field($model, 'password')->passwordInput([
            'maxlength' => 100,
            'id' => 'password',
            'placeholder' => 'Пароль'
        ]) ?>
        <?= $form->field($model, 'repeat')->passwordInput([
            'maxlength' => 100,
            'id' => 'repeat',
            'placeholder' => 'Повторите пароль'
        ]) ?>
    </div>
    <div class="pair">
        <?= $form->field($model, 'email')->textInput([
            'maxlength' => 64,
            'id' => 'email',
            'placeholder' => 'Email'
        ]) ?>
        <?= $form->field($model, 'skype')->textInput([
            'maxlength' => 32,
            'id' => 'skype',
            'placeholder' => 'Skype'
        ]) ?>
    </div>

    <div class="regbutton">
        <label>
            <input type="checkbox" required/>
            я знаком и согласен с
            <?= Html::a('правилами', '/rules') ?>
        </label>
        <br/>
        <?= Html::submitButton(Yii::t('app', 'Signup'),
            ['class' => 'button full-width']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</article>
