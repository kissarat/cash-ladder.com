<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\form\Login */
/* @var $form ActiveForm */
?>

<section id="page-login" class="section">
    <h1><?= Yii::t('app', 'Login') ?></h1>
    <?php $form = ActiveForm::begin(['options' => ['class' => 'login-form'],]); ?>
    <div class="first">
        <?= $form->field($model, 'login')
            ->textInput([
                'maxlength' => 100,
                'id' => 'username',
                'class' => 'form-control',
                'placeholder' => 'Логин'
            ])
            ->label(false) ?>
        <?= $form->field($model, 'password')
            ->passwordInput([
                'maxlength' => 100,
                'id' => 'password',
                'class' => 'form-control',
                'placeholder' => 'Пароль'
            ])
            ->label(false) ?>
    </div>

    <div>
        <?= Html::a('Забыли пароль?', ['user/request']) ?>
        <?= Html::submitButton(Yii::t('app', 'Login'),
            ['class' => 'button full-width']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</section>
