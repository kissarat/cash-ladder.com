<?php
use app\helpers\Html;
use yii\helpers\Url;

/* @var $model array */

$profileData = json_encode($model);
echo Html::script("const userProfile = $profileData;");

extract($model);

$referral = Url::to(['user/signup', 'sponsor' => $nick], true);

$isMe = Yii::$app->user->getIsGuest() ? false : Yii::$app->user->identity->nick === $nick;
?>

<article id="page-profile" xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <section class="">
        <ul>
            <li>
                <span>Логин:</span>
                <span><?= $nick ?></span>
            </li>
            <?php if (!empty($parent)): ?>
                <li>
                    <span>Спонсор:</span>
                    <span><?= $parent->nick ?></span>
                </li>
            <?php endif; ?>
            <li>
                <span>Баланс:</span>
                <span><?= Html::money($balance) ?></span>
            </li>
            <li>
                <span>Парт. ссылка:</span>
                <?= Html::textInput('reflink', $referral) ?>
            </li>
        </ul>
    </section>
    <section class="days">
        <div v-if="days >= 0">
            <div class="text">Осталось дней до покупки: {{ days }}</div>
            <input type="hidden" name="expires" value="<?= $expires ?>"/>
            <div class="list">
                <?php
                foreach (range(1, 7) as $number):
                    $day = strtotime($expires) + $number * 24 * 3600;
                    ?>
                    <div data-expires="<?= $day ?>"
                         v-bind:class="{
                         day: true,
                         <?= $number ?>: true,
                         available: <?= $day > time() ? 'true' : 'false' ?>
                     }">
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
        <div v-else>
            <div>Нет активных покупок</div>
        </div>
    </section>
    <section class="programs">
        <h2>Тарифы</h2>
        <div>
            <?php foreach ($programs as $id => $price): ?>
                <?= Html::beginTag('a', [
                    'style' => ['margin-top' => 360 - $id * 60],
                    'href' => Url::to(['program/index',
                        'id' => $id,
                        'nick' => Yii::$app->user->identity->nick
                    ])
                ]) ?>
                <img src="/images/<?= $id ?>.png">
                <div>
                    <div class="h-stupen">Ступень <?= $id ?></div>
                    <div class="eur"><?= $price ?></div>
                </div>
                <?= Html::endTag('a') ?>
            <?php endforeach; ?>
        </div>
    </section>
</article>
