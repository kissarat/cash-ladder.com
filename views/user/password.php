<?php

use yii\authclient\widgets\AuthChoice;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\Password
 * @var $form ActiveForm
 */

$reset = !$model || 'reset' == $model->scenario;
$this->title = Yii::t("app", $reset ? 'Reset Password' : 'Set Password');

if ($message): ?>
    <div class="alert alert-danger"><?= $message ?></div>
<?php else: ?>
    <article class="section" id="page-password" class="<?= $reset ? 'reset-password' : 'set-password' ?>">
        <h1><?= $this->title ?></h1>
        <?php $form = ActiveForm::begin(); ?>

        <?php if (!$reset) {
            echo $form->field($model, 'password')->passwordInput();
        } ?>
        <?= $form->field($model, 'new_password')->passwordInput() ?>
        <?= $form->field($model, 'repeat_password')->passwordInput() ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </article>
<?php
endif;
