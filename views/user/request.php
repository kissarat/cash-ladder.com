<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $message string
 * @var $model app\models\form\RequestPasswordReset
 * @var $form ActiveForm
 */

$this->title = Yii::t('app', 'Password Recovery Request');
?>

<section id="page-request" class="section">
    <?php $form = ActiveForm::begin(); ?>
    <h1><?= Yii::t('app', 'Reset password') ?></h1>

    <?= $form->field($model, 'email')->textInput([
        'maxlength' => 100,
        'id' => 'email',
        'placeholder' => 'Email'
    ]) ?>
    
    <div class="regbutton">
        <?= Html::submitButton(Yii::t('app', 'Send Email'),
            ['class' => 'button full-width']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</section>
