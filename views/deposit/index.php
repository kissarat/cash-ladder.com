<?php
use app\helpers\Html;
use yii\widgets\ActiveForm;

$deposits = [];
foreach ($models as $model) {
    $deposits[] = [
        'id' => $model->id,
        'name' => $model->name,
        'min' => $model->min,
        'max' => $model->max,
        'quantity' => $model->quantity,
        'accrue' => $model->accrue,
    ];
}

echo Html::script('const deposits = ' . json_encode($deposits))
?>

<div id="page-deposit-index"
     xmlns:v-on="http://www.w3.org/1999/xhtml"
     xmlns:v-bind="http://www.w3.org/1999/xhtml">
    <section>
        <h1>Депозиты</h1>
        <ul>
            <li v-for="d of deposits"
                v-bind:class="{current: d.id === id}"
                v-on:click="select(d.id)">
                <h4>{{ d.name }}</h4>
                <div>
                    <span class="eur-sign">{{ d.min / 100 }}</span>
                </div>
                <div>{{ d.quantity }} дней</div>
                <div class="percent">{{ (d.accrue * 100).toFixed(1) }}</div>
            </li>
        </ul>
        <?php $form = ActiveForm::begin() ?>
        <input name="amount"
               v-bind:value="euro"
               v-on:input="setEuro($event.target.value)"
               v-bind:min="min"
               type="number"/>
        <div v-if="valid">
            <input type="hidden" name="id" v-bind:value="id" required/>
            <button>Kупить</button>
        </div>
        <?= Html::hiddenInput('amount', null, ['v-bind:value' => 'amount']) ?>
        <?php ActiveForm::end() ?>
        <div v-if="valid">
            <div class="profit">
                <span>Прибыль:</span>
                <span class="eur">{{ profit }}</span>
            </div>
            <div class="finish">
                <span>Дата закрытия:</span>
                <span class="date">{{ finish }}</span>
            </div>
        </div>
    </section>
</div>
