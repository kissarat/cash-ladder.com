<?php
use app\helpers\Html;
use app\models\Transfer;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

$focus = empty($_GET['id']) ? null : +$_GET['id'];
$nick = Yii::$app->user->identity->nick;

function param($key, $value = null, $text = null) {
    $params = $_GET;
    if ($value) {
        $params[$key] = $value;
    } else {
        unset($params[$key]);
    }
    $params[0] = 'transfer/index';
    return Html::a(Yii::t("app", $text) ?: $value, $params);
}

function params($label, $key, $values) {
    $result = [param($key, null, 'All')];
    foreach ($values as $value => $text) {
        $result[] = param($key, is_numeric($value) ? $text : $value, $text);
    }
    return Html::tag('span',
        Html::tag('span', Yii::t("app", $label), ['class' => 'label'])
        . Html::tag('span', implode(' | ', $result), ['class' => 'values ' . $key]), [
            'id' => 'filter-' . $key
        ]);
}

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>
<article id="page-transfers" class="section">
    <h1>Мои финансы</h1>

    <div class="ticker">
        <?php
        for ($i = 0; $i < count($ticker); $i++) {
            $text = $ticker[$i];
            echo "<div v-bind:class=\"{active: $i === ticker}\">$text</div>";
        }
        ?>
    </div>

    <?php $form = ActiveForm::begin(['id' => 'form-payment']) ?>
    <?= $form->field($payment, 'amount')
        ->textInput(['placeholder' => 'Сумма'])
        ->label('Пополнение') ?>
    <div>
        <?= $form
            ->field($payment, 'system')
            ->hiddenInput(['v-model' => 'system'])
            ->label(false) ?>

        <?php foreach (Transfer::$systems as $id => $name): ?>
            <?= Html::button($name, ['name' => $id, 'v-on:click' => "pay('$id')"]) ?>
        <?php endforeach; ?>
        <?= Html::a('Withdraw', ['transfer/withdraw']) ?>
    </div>
    <?php ActiveForm::end() ?>
</article>
<article id="page-transfers" class="section">
    <!--    <?= params('Type: ', 'type', [
        'payment' => 'Payment',
        'withdraw' => 'Withdrawn',
        'accrue' => 'Accruals',
        'bonus' => 'Referral',
    ]) ?> -->

    <?= Yii::t('app', 'Payment from {wallet}', ['wallet' => 'abc']) ?>

    <?= GridView::widget([
        'dataProvider' => $provider,
        'layout' => '<div class="table-control2">{errors}</div>{items}{summary}{pager}',
        'tableOptions' => ['class' => 'striped bordered'],
        'emptyText' => 'Nothing found',
        'rowOptions' => function (Transfer $model) use ($focus) {
            $class = $focus === $model->id ? 'focus' : 'normal';
            return ['class' => $class];
        },
        'columns' => [
            'id',
            [
                'attribute' => 'created',
                'label' => Yii::t('app', 'Time'),
                'format' => 'datetime',
                'contentOptions' => function (Transfer $model) {
                    return ['data-time' => strtotime($model->created)];
                }
            ],
            [
                'attribute' => 'amount',
                'format' => 'html',
                'value' => function (Transfer $model) {
                    $sign = $model->to === Yii::$app->user->identity->user ? '+' : '-';
                    $currency = strtolower($model->currency);
                    return "<span class=\"sign\">$sign</span><span class=\"$currency\">$model->amount</span>";
                }
            ],
            [
                'attribute' => 'text',
                'label' => Yii::t('app', 'Description'),
                'value' => function (Transfer $model) {
                    return $model->getDescription();
                }
            ],
            [
                'attribute' => 'status',
                'label' => Yii::t('app', 'Status'),
                'value' => function (Transfer $model) {
                    switch ($model->status) {
                        case 'success':
                            return "\u{2713}";

                        case 'fail':
                            return "\u{274C}";

                        case 'canceled':
                            return "Canceled";

                        case 'created':
                            return 'Created';
                        default:
                            return $model->status;
                    }
                }
            ],
        ]
    ]) ?>
</article>
