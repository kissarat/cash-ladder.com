<?php
use app\models\Transfer;
use yii\widgets\ActiveForm;
?>

<section class="section transfer-withdraw">
    <?php $form = ActiveForm::begin() ?>
    <?= $form->field($model, 'system')->dropDownList(Transfer::$systems) ?>
    <?= $form->field($model, 'amount')->textInput(['maxlength' => 12, 'id' => 'amount']) ?>
    <?= $form->field($model, 'wallet')->textInput(['maxlength' => 128, 'id' => 'wallet']) ?>
    <?= $form->field($model, 'text')->textInput(['maxlength' => 255, 'id' => 'text']) ?>

    <button type="submit" class="btn btn-success">
        <?= Yii::t('app', 'Create') ?>
    </button>
    <?php ActiveForm::end() ?>
</section>
