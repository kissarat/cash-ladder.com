<?php
use app\helpers\Html;
use yii\grid\GridView;

require '_grid.php';

/**
 * @var yii\data\ArrayDataProvider $provider
 */
?>
<section class="structure-index" id="content">
    <h1>Партнеры</h1>
    <?= GridView::widget(configGrid([
        'dataProvider' => $provider,
        'layout' => '{items}{pager}',
//                    'filterModel' => $search,
        'tableOptions' => ['class' => 'table'],
    ])) ?>
</section>
