<?php

function configGrid($config) {
    return array_merge_recursive([
        'emptyText' => 'Nobody',
        'rowOptions' => function ($model) {
            return ['data-id' => $model->id];
        },
        'columns' => [
//            [
//                'attribute' => 'children',
//                'contentOptions' => function ($model) {
//                    return ['data-children' => $model->children];
//                }
//            ],
            'nick',
//            [
//                'attribute' => 'created',
//                'label' => Yii::t('app', 'Registered'),
//                'format' => 'date',
//                'contentOptions' => function ($model) {
//                    return ['data-time' => strtotime($model->created)];
//                }
//            ],
//            'surname',
//            'forename',
            [
                'attribute' => 'level',
                'label' => Yii::t('app', 'Level'),
                'contentOptions' => ['class' => 'level'],
                'value' => function ($model) {
                    return $model->level > 0 ? $model->level : 'Sponsor';
                }
            ]
//            [
//                'attribute' => 'eur',
//                'label' => Yii::t('app', 'Invested'),
//                'contentOptions' => ['class' => 'eur'],
//            ],
//            [
//                'attribute' => 'sponsor',
//                'label' => Yii::t('app', 'Sponsor'),
//                'value' => function ($model) {
//                    return $model->sponsor !== Yii::$app->user->identity->nick ? $model->sponsor : '';
//                }
//            ]
        ]
    ],
        $config);
}
