<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Profile');
?>

<section id="page-settings-profile">
    <h1><?= Yii::t('app', 'Change Profile Settings') ?></h1>



    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    <div>
        <?= $form->field($model, 'forename')
            ->textInput(['maxlength' => 48, 'id' => 'username']) ?>

        <?= $form->field($model, 'surname')
            ->textInput(['maxlength' => 48, 'id' => 'surname']) ?>
    </div>
    <div>
        <?= $form->field($model, 'email')
            ->textInput(['maxlength' => 48, 'id' => 'email']) ?>

        <?= $form->field($model, 'skype')
            ->textInput(['maxlength' => 32, 'id' => 'skype']) ?>
    </div>

    <!-- <div id="upload_avatar">
        <?= $form->field($model, 'avatar_file')
        ->fileInput([
            'accept' => 'image/*'
        ]) ?>
    </div> -->

    <div class="links">
        <?= Html::a('Изменить пароль', ['settings/password', 'nick' => Yii::$app->user->identity->nick]) ?>
    </div>

    <div class="regbutton2">
        <?=
        Html::submitButton(Yii::t('app', 'Save'),
            ['class' => 'width100']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</section>
