<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = Yii::t('app', 'Wallets');
?>

<section id="content">
    <?php require '_breadcrumbs.php' ?>

    <div class="container">
        <div class="section">
            <?php require '_menu.php' ?>

            <div class="row">
                <div class="col s12 m12 l12">
                    <div class="card-panel">
                        <h4 class="header2"><?= Yii::t('app', 'Specify your wallets') ?></h4>
                        <?= Yii::t('app', 'You can change wallets through technical support only') ?>
                        <?php $form = ActiveForm::begin(); ?>

                        <div class="row margin">

                            <?= $form->field($model, 'perfect', [
                                'inputOptions' => ['disabled' => !!$model->perfect, 'class' => 'form-control'],
                                'template' => '
                    <div class="input-field col s6">
                        <i class="mdi-action-lock-outline prefix"></i>{input}{label}{error}{hint}</div>'
                            ])->textInput(['maxlength' => 255, 'id' => 'perfect', 'class' => ''])
                            ?>
                        </div>

                        <!--      <?= $form->field($model, 'perfect',
                            ['inputOptions' => ['disabled' => !!$model->perfect, 'class' => 'form-control']]) ?> -->


                        <div class="row">
                            <div class="input-field col s12">
                                <?= Html::submitButton(Yii::t('app', 'Save') . '<i class="mdi-content-send right"></i>',
                                    ['class' => 'btn cyan waves-effect waves-light right']) ?>
                            </div>
                        </div>

                        <?php ActiveForm::end(); ?>

                        <div class="alert alert-warning" role="alert">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
