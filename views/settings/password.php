<?php

use app\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('app', 'Password');
?>

<section id="page-password">
    <h1><?= Yii::t('app', 'Password Change') ?></h1>



    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'current')->passwordInput(['maxlength' => 255, 'id' => 'current']) ?>
    <?= $form->field($model, 'new')->passwordInput(['maxlength' => 255, 'id' => 'new']) ?>
    <?= $form->field($model, 'repeat')->passwordInput(['maxlength' => 255, 'id' => 'repeat']) ?>
    <div class="links">
        <?= Html::a('Настройки профиля', ['settings/profile', 'nick' => Yii::$app->user->identity->nick]) ?>
    </div>
    <br>
    <br>
    <?= Html::submitButton(Yii::t('app', 'Change')) ?>
    <?php ActiveForm::end(); ?>
</section>
