const https = require('https')
const fs = require('fs')

const entites = ''

function fetch(path) {
  return new Promise(function (resolve, reject) {
    const req = https.request({
          hostname: 'etherscan.io',
          method: 'GET',
          path,
          headers: {
            accept: 'application/json',
          }
        },
        function (res) {
          const chunks = []
          res.on('data', function (data) {
            chunks.push(data)
          })
          res.on('end', function () {
            resolve(Buffer.concat(chunks).toString('utf8'))
          })
        })
    req.on('error', reject)
    req.end()
  })
}

async function page(number) {
  const list = await fetch('/contractsVerified/' + number)
  if (list.indexOf('Ray ID:') > 0) {
    console.error('CloudFlare protection')
    process.exit(1)
  }
  const regex = /\/address\/(0x[0-9a-f]{40})/mg
  let address
  while (address = regex.exec(list)) {
    const html = await fetch('/address/' + address[1])
    const data = {}
    const name = /Contract Name:\n<\/td>\n<td>([^<]+)/.exec(html)
    if (name) {
      data.name = name[1].trim()
    }
    const compiler = /Compiler Version:\n<\/td>\n<td>([^<]+)/.exec(html)
    if (compiler) {
      data.compiler = compiler[1].trim()
    }
    const solidity = /solidity[\s^]*([\d.]+);/.exec(html)
    if (solidity) {
      data.solidity = solidity[1].trim()
    }
    const pre = /<pre[^>]+>([^<]+)<\/pre>/mg
    const sources = []
    let source
    while (source = pre.exec(html)) {
      sources.push(source[1])
    }
    data.address = address[1]
    data.swarm = sources[3]
    data.source = sources[0]
    try {
      data.abi = JSON.parse(sources[1])
    }
    catch (ex) {
      console.error('Cannot parse abi', data.address, sources[1])
    }
    fs.writeFileSync(__dirname + `/contracts/${data.address}.json`, JSON.stringify(data))
  }
}

async function run() {
  for(let i = 379; i > 0; i--) {
    await page(i)
  }
}

void run()
