<?php

namespace app;


class Request extends \yii\web\Request {
    public function getUserIP() {
        return empty($_SERVER['HTTP_IP']) ? parent::getUserIP() : $_SERVER['HTTP_IP'];
    }
}
