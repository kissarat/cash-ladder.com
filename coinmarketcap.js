const https = require('https')

function fetch() {
  const req = https.request({
        hostname: 'api.coinmarketcap.com',
        method: 'GET',
        path: '/v1/ticker/?limit=100',
        headers: {
          accept: 'application/json',
        }
      },
      function (res) {
        const chunks = []
        res.on('data', function (data) {
          chunks.push(data)
        })
        res.on('end', function () {
          console.log(JSON.stringify(JSON.parse(Buffer.concat(chunks).toString('utf8'))))
        })
      })
  req.on('error', function (err) {
    console.error(err)
  })
  req.end()
}

fetch()
setInterval(fetch, 10000)
