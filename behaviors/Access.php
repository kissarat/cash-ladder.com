<?php

namespace app\behaviors;


use app\models\Log;
use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\UnauthorizedHttpException;

class Access extends Behavior {
    public $admin;
    public $plain;
    public $guest;

    public function events() {
        return [
            Controller::EVENT_BEFORE_ACTION => 'beforeAction'
        ];
    }

    public function beforeAction(ActionEvent $event) {
        $event->handled = !$this->check($event->action->id);
        if ($event->handled) {
            throw new UnauthorizedHttpException();
        }
    }

    public function check($action) {
        $user = Yii::$app->user;
        $roles = ['admin', 'plain', 'guest'];
        $role = null;

        foreach($roles as $role) {
            $actions = $this->$role;
            if (is_array($actions) && in_array($action, $actions)) {
                break;
            }
        }

        if ('guest' == $role) {
            return true;
        }
        if (!$user->identity || !$user->identity->nick) {
            return false;
        }
        if ('plain' == $role) {
            return true;
        }
        if ($user->identity->isAdmin()) {
            return true;
        }
        return false;
    }

    public static function verifyNick() {
        $nick = isset($_GET['nick']) ? $_GET['nick'] : null;
        if ($nick !== Yii::$app->user->identity->nick) {
            Log::log('deny', Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, ['nick' => $nick]);
            throw new ForbiddenHttpException();
        }
    }

    public static function verifyUser() {
        $user = isset($_GET['user']) ? +$_GET['user'] : null;
        if ($user !== Yii::$app->user->identity->nick) {
            Log::log('deny', Yii::$app->controller->id . '/' . Yii::$app->controller->action->id, ['user' => $user]);
            throw new ForbiddenHttpException();
        }
    }
}
