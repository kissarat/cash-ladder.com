<?php

namespace app\controllers;


use app\helpers\SQL;
use app\models\form\Generator;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class GenerateController extends Controller {
    public function beforeAction($action) {
        if (!Yii::$app->user->getIsGuest() && Yii::$app->user->identity->admin) {
            return parent::beforeAction($action);
        }
        throw new ForbiddenHttpException();
    }

    public function actionUser() {
        $model = new Generator();
        $errors = null;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
//            throw new \Exception(json_encode($model->attributes, JSON_PRETTY_PRINT));
            $errors = $model->generate();
        }
        return $this->render('user', ['model' => $model, 'errors' => $errors]);
    }

    public function actionClear() {
        SQL::execute('DELETE FROM node WHERE id IN (SELECT id FROM node WHERE id >= 20 ORDER BY id DESC)');
        SQL::execute('DELETE from transfer WHERE id > 1');
        SQL::execute('DELETE from "user" WHERE id > 7');
        return $this->redirect(['generate/user']);
    }
}
