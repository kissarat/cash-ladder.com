<?php

namespace app\controllers;


use app\helpers\SQL;
use yii\web\NotFoundHttpException;

class ArticleController extends RestController
{
    public function actionIndex() {
        return $this->render('index', [
            'models' => SQL::queryAll("SELECT id, name, path, short FROM article WHERE type = 'article'")
        ]);
    }

    public function actionView($path) {
        $article = SQL::queryByKey('article', $path, 'path');
        if ($article) {
            return $this->render('view', [
                'model' => $article
            ]);
        }
        throw new NotFoundHttpException($path);
    }
}
