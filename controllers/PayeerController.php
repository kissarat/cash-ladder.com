<?php

namespace app\controllers;


use app\models\Transfer;
use app\controllers\base\YarcodeController;
use yarcode\payeer\events\GatewayEvent;
use Yii;

class PayeerController extends YarcodeController
{
    public function getPaymentSystemId() {
        return 'payeer';
    }

    public function onStatus(GatewayEvent $e) {
        $txid = $e->gatewayData['m_operation_id'];
        $vars = $e->gatewayData;
        $vars['agent'] = Yii::$app->request->getUserAgent();
        if (Transfer::find()->where(['txid' => $txid])->count() <= 0) {
            $t = new Transfer([
                'amount' => $e->gatewayData['m_amount'] * 100,
                'currency' => $e->gatewayData['m_curr'],
                'status' => 'success' === $e->gatewayData['m_status'] ? 'success' : 'fail',
                'txid' => $txid,
                'to' => Yii::$app->user->identity->user,
                'type' => 'payment',
                'ip' => Yii::$app->request->getUserIP(),
                'vars' => json_encode($vars)
            ]);
            $t->save(false);
        }
        $e->handled = true;
    }
}
