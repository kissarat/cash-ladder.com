<?php

namespace app\controllers\base;


use app\behaviors\Access;
use app\models\Transfer;
use yarcode\payeer\events\GatewayEvent;
use Yii;
use yii\web\Controller;
use yii\web\Response;

abstract class YarcodeController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['pay', 'status']
            ]
        ];
    }

    /**
     * @return string
     */
    public abstract function getPaymentSystemId();

    /**
     * @return \yii\base\Component
     */
    public function getComponent() {
        return Yii::$app->get($this->getPaymentSystemId());
    }

    public function init() {
        parent::init();
        $this->getComponent()->on(GatewayEvent::EVENT_PAYMENT_REQUEST, [$this, 'onStatus']);
    }

    public function actionPay($amount) {
        $transfer = new Transfer([
            'type' => 'payment',
            'status' => 'created',
            'system' => $this->getPaymentSystemId(),
            'to' => Yii::$app->user->identity->user,
            'ip' => Yii::$app->request->getUserIP(),
            'amount' => (int)$amount
        ]);
        $transfer->save();
        return $this->render('pay', [
            'id' => $transfer->id,
            'amount' => $amount / 100,
            'description' => ''
        ]);
    }

    public function actionStatus() {
        if ($this->getComponent()->processResult(Yii::$app->request->get())) {
            return $this->redirect(['transfer/index']);
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return Yii::$app->request->get();
    }
}
