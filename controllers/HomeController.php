<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\helpers\Utils;
use app\models\Coordinate;
use Yii;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class HomeController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'admin' => ['mail']
            ]
        ];
    }

    public function beforeAction($action) {
        Yii::$app->controller->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionError() {
        $exception = Yii::$app->getErrorHandler()->exception;
        if ($exception instanceof HttpException) {
            if ($exception instanceof NotFoundHttpException) {
                $path = substr($_SERVER['REQUEST_URI'], 1);
                $article = SQL::queryByKey('article', $path, 'path');
                if ($article) {
                    return $this->render('@app/views/article/view', ['model' => $article]);
                }
            }
            if ($exception instanceof UnauthorizedHttpException && !$this->isApiCall()) {
                return $this->redirect(['user/login']);
            }
            $this->view->title = Yii::t("app", $exception->getName());
            return $this->render('error', [
                'status' => $exception->statusCode,
                'error' => [
                    'message' => $exception->getMessage() ?: $exception->getName()
                ]
            ]);
        }
        return $this->render('exception', [
            'exception' => $exception
        ]);
    }

    public function actionAdmin() {
        return $this->render('admin');
    }

    public function actionCoordinate($time = null, $latitude, $longitude, $altitude = null, $accuracy = null) {
//        Yii::$app->response->headers->add('Cache-Control', 'max-age=3600, public');
        if (!$time) {
            $time = time();
        }
        $time = Utils::timestamp($time / 1000);
        Yii::$app->response->format = Response::FORMAT_JSON;
        $coordinate = new Coordinate([
            'time' => $time,
            'latitude' => $latitude,
            'longitude' => $longitude,
            'altitude' => $altitude,
            'accuracy' => $accuracy
        ]);
        $coordinate->save();
        return [
            'result' => ['id' => $coordinate->id]
        ];
    }

    public function actionDefault() {
        return $this->redirect(Yii::$app->user->getIsGuest()
            ? ['user/login']
            : ['user/profile', 'nick' => Yii::$app->user->identity->nick]);
    }

    public function actionMail() {
        Utils::mail($_POST['to'], $_POST['subject'], $_POST['content']);
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionVisit($uid, $url, $spend) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        SQL::execute('INSERT INTO visit(token, url, spend, ip, agent) VALUES (:uid, :url, :spend, :ip, :agent)', [
            ':uid' => $uid,
            ':url' => base64_decode($url),
            ':spend' => $spend,
            ':ip' => Yii::$app->request->getUserIP(),
            ':agent' => Yii::$app->request->getUserAgent(),
        ]);
        return Utils::jsonp(['success' => true]);
    }

    public function actionNewbie($nick, $ico = null) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $token = null;
        if (isset($_COOKIE['PHPSESSID'])) {
            $token = $_COOKIE['PHPSESSID'];
        } else if (isset($_COOKIE['ico'])) {
            $token = $_COOKIE['ico'];
        } else if ($ico) {
            $token = $ico;
        }
        $last = null;
        if ($token) {
            $last = SQL::scalar('SELECT created FROM visit WHERE token = :token LIMIT 1', ['token' => $token]);
        }
        if (!$token || !$last) {
            SQL::execute('INSERT INTO visit(token, url, ip, agent) VALUES (:uid, :url, :ip, :agent)', [
                ':uid' => $token,
                ':url' => '/ref/' . $nick,
                ':ip' => Yii::$app->request->getUserIP(),
                ':agent' => Yii::$app->request->getUserAgent(),
            ]);
        }
        return Utils::jsonp([
            'success' => true,
            'last' => $last
        ]);
    }

    public function actionStatus() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['success' => true];
    }

    public function actionSitemap() {
        Yii::$app->response->format = Response::FORMAT_RAW;
        $articles = SQL::queryAll('SELECT path, created FROM article WHERE path IS NOT NULL');
        $this->layout = false;
        Yii::$app->response->getHeaders()->add('Content-Type', 'text/xml; charset=utf-8');
        return $this->render('sitemap', ['articles' => $articles]);
    }
}
