<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\helpers\Utils;
use app\models\form\Payment;
use app\models\form\Withdraw;
use app\models\Log;
use app\models\Transfer;
use app\models\User;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Response;

class TransferController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'pay', 'withdraw', 'address']
            ]
        ];
    }

    public function beforeAction($action) {
//        Access::verifyNick();
        return parent::beforeAction($action);
    }

    public function actionIndex() {
        $user = Yii::$app->user->identity->user;
        $query = Transfer::find()
            ->where(['or', ['from' => $user], ['to' => $user]])
            ->andFilterWhere(Utils::pick($_GET, ['currency', 'type']));
        $provider = new ActiveDataProvider([
            'query' => $query
        ]);

        $amount = Yii::$app->request->get('amount');
        $payment = new Payment();
        if ($amount) {
            $payment->amount = $amount / 100;
        }
        if ($payment->load(Yii::$app->request->post()) && $payment->validate()) {
            return $this->redirect([$payment->system . '/pay', 'amount' => $payment->amount * 100]);
        }

        return $this->render('index', [
            'payment' => $payment,
            'provider' => $provider,
            'ticker' => Transfer::getTicker()
        ]);
    }

    public function actionIncome() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity->user;
        $transfers = SQL::queryAll('SELECT * FROM transfer WHERE "to" = :user AND type = \'accrue\' ORDER BY id DESC LIMIT 6',
            [':user' => $user]);
        return ['result' => $transfers];
    }

    public function actionWithdraw() {
        $user_id = Yii::$app->user->identity->user;
        $transfer = Transfer::findOne([
            'from' => $user_id,
            'type' => 'withdraw',
            'status' => 'created'
        ]);
        if ($transfer) {
            Yii::$app->session->setFlash('error',
                Yii::t('app', 'You cannot create more than one withdrawal request'));
            Log::log('withdraw', 'second');
            return $this->redirect(['transfer/index', 'id' => $transfer->id]);
        }
        $model = new Withdraw();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $amount = $model->amount * 100;
            $t = Yii::$app->db->beginTransaction();
            $currency = Transfer::$currencies[$model->system];
            $min = Withdraw::$minimal[$currency];
//            header('H:' . $currency . $min);
            if ($amount >= $min) {
                $balance = User::getBalance($currency);
                if ($balance - $amount >= 0) {
                    $transfer = new Transfer([
                        'type' => 'withdraw',
                        'status' => 'created',
                        'currency' => $currency,
                        'from' => $user_id,
                        'amount' => $amount,
                        'wallet' => $model->wallet,
                        'text' => empty($model->text) ? null : $model->text
                    ]);
                    if ($transfer->save(false)) {
                        $commission = SQL::scalar('SELECT amount FROM need_commission WHERE "user" = :user', [
                            ':user' => $user_id
                        ]);
                        if ($commission > 0) {
                            $c = new Transfer([
                                'type' => 'commission',
                                'status' => 'success',
                                'currency' => $currency,
                                'from' => $user_id,
                                'amount' => $commission,
                                'vars' => json_encode(['withdraw' => $transfer->id]),
                                'text' => empty($model->text) ? null : $model->text
                            ]);
                            $c->save(false);
                        }
                        Yii::$app->session->setFlash('success',
                            Yii::t('app', 'Withdrawal request created'));
                        $t->commit();
                        return $this->redirect(['transfer/index', 'id' => $transfer->id]);
                    }
                } else {
                    Log::log('withdraw', 'insufficient', [
                        'balance' => $balance,
                        'amount' => $amount,
                        'wallet' => $model->wallet,
                        'currency' => $currency
                    ]);
                    $model->addError('amount', Yii::t('app', 'Insufficient funds'));
                }
                $t->rollBack();
            } else {
                $model->addError('amount', Yii::t('app', 'Minimal amount is {amount}', [
                    'amount' => '0.01 ETH'
                ]));
            }
        }
        return $this->render('withdraw', [
            'model' => $model
        ]);
    }

    public function actionHistogram($user, $types, $start, $currencies = 'USD', $step = '1 day', $stop) {
        Yii::$app->response->format = Response::FORMAT_JSON;
//        Yii::$app->response->headers->add('Cache-Control', 'max-age=3600, public');
        $types = explode(',', $types);
        $currencies = explode(',', $currencies);
        $params = [
            ':start' => $start,
            ':step' => $step,
            ':user' => $user,
            ':stop' => $stop
        ];
        $many = function ($array) use (&$params) {
            $pp = [];
            foreach ($array as $el) {
                $p = ':p' . count($params);
                $params[$p] = $el;
                $pp[] = $p;
            }
            return implode(',', $pp);
        };

        $pType = $many($types);
        $pCurrencies = $many($currencies);

        $histogram = SQL::queryAll("SELECT
        currency,
        type,
        s AS time,
        sum(coalesce(amount, 0)) AS amount
  FROM generate_series(:start::TIMESTAMP, :stop::TIMESTAMP, :step::INTERVAL) s LEFT JOIN user_transfer t
      ON
        t.created >= s
        AND t.created < s + :step::INTERVAL
        AND currency IN ($pCurrencies)
        AND type IN ($pType)
        AND \"user\" = :user
  GROUP BY s, type, currency ORDER BY time", $params);
        $result = [];
        foreach ($histogram as $m) {
            $time = $m['time'];
            if (!isset($result[$time])) {
                $result[$time] = [];
            }
            if (null !== $m['currency']) {
                unset($m['time']);
                $result[$time][] = $m;
            }
        }
        // Remove invalid data
//        array_pop($result);
        return ['result' => $result];
    }
}
