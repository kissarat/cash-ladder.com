<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\Referral;
use app\models\search\ReferralSearch;
use app\models\Visit;
use PDO;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Response;

class StructureController extends RestController {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'children']
            ]
        ];
    }

    public function actionIndex() {
        $search = new ReferralSearch();
        return $this->render('index', [
            'search' => $search,
            'provider' => $search->search($_GET, [
//                'sort' => [
//                    'defaultOrder' => [
//                        'level' => SORT_ASC,
//                        'id' => SORT_DESC
//                    ]
//                ]
            ])
        ]);
    }

    public function actionVisit() {
        Yii::$app->response->format = Response::FORMAT_JSONP;
        $visit = new Visit([
            'url' => '/ref/' . Yii::$app->request->get('r'),
            'ip' => Yii::$app->request->getUserIP()
        ]);
        $visit->save();
        return [
            'callback' => Yii::$app->request->get('callback') ?: 'callback',
            'data' => ['ok' => 1]
        ];
    }
}
