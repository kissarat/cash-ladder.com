<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\MatrixNode;
use app\models\Node;
use app\models\Program;
use PDO;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\db\Transaction;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class ProgramController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index']
            ]
        ];
    }

    public function actionIndex() {
        $program_id = +Yii::$app->request->post('id') ?: +Yii::$app->request->get('id');
        $user_id = Yii::$app->user->identity->user;
        $provider = null;
        if ('POST' === Yii::$app->request->method) {
            $tx = Yii::$app->db->beginTransaction(Transaction::SERIALIZABLE);

            $node_id = Program::buy($program_id, $user_id, true);
            if ($node_id <= 0) {
                $tx->rollBack();
                if (-402 === $node_id) {
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Insufficient funds'));
                    return $this->redirect(['transfer/index', 'nick' => Yii::$app->user->identity->nick]);
                }
                throw new ServerErrorHttpException('Internal Error: ' . $node_id);
            } else {
                $tx->commit();
            }
        }
        $where = ['user' => $user_id];
        if ($program_id > 0) {
            $where['program'] = $program_id;
        }
        $provider = new ActiveDataProvider([
            'query' => MatrixNode::find()
                ->where($where)
                ->select(['id', 'count', 'program', 'created'])
        ]);
        return $this->render('index', [
            'program_id' => $program_id,
            'provider' => $provider,
            'programs' => [
                1 => 300,
                2 => 500,
                3 => 1500,
                4 => 4100,
                5 => 12000,
                6 => 30000
            ]
        ]);
    }

    public function actionUser($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return ['result' => SQL::queryAll('SELECT nick FROM node_user WHERE parent = :parent', [
            ':parent' => $id
        ], PDO::FETCH_COLUMN)];
    }
}
