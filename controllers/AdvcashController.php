<?php

namespace app\controllers;

use app\behaviors\Access;
use app\helpers\Utils;
use app\models\Log;
use app\models\Transfer;
use yarcode\advcash\Merchant;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class AdvcashController extends Controller
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['pay']
            ]
        ];
    }

    /** @inheritdoc */
    public $enableCsrfValidation = false;

    /** @var string Your component configuration name */
    public $componentName = 'advcash';

    public function actionSuccess() {
        return $this->handle(Yii::$app->request->get());
    }

    public function actionFailure() {
        return $this->handle(Yii::$app->request->get());
    }

    public function actionResult() {
        return $this->handle(Yii::$app->request->get());
    }

    protected function handle($data) {
        $transfer = Transfer::findOne(ArrayHelper::getValue($data, 'ac_order_id'));
        /**
         * @var $m Merchant
         */
        $m = Yii::$app->get($this->componentName);

        if ($transfer && isset($data['ac_hash']) && $m->checkHash($data) && ArrayHelper::getValue($data, 'ac_sci_name') == $m->merchantName) {
            $amount = ArrayHelper::getValue($data, 'ac_amount');
            $transfer->wallet = ArrayHelper::getValue($data, 'ac_src_wallet');
            $transfer->txid = ArrayHelper::getValue($data, 'ac_transfer');
            $transfer->amount = $amount * 100;
            $transfer->status = ArrayHelper::getValue($data, 'ac_transaction_status') == Merchant::TRANSACTION_STATUS_COMPLETED
                ? 'success' : 'fail';
            $transfer->time = Utils::timestamp();
            $transfer->save(false);
            $nick = Yii::$app->user->identity->nick;
            Yii::$app->telegram->sendText("Payment from $nick \$$amount $transfer->wallet");
        } else {
            Log::log('advcash', 'merchant', $data);
        }

        return $this->redirect(['transfer/index',
            'nick' => Yii::$app->user->identity->nick,
            'id' => $transfer->id
        ]);
    }

    public function actionPay($amount) {
        $transfer = new Transfer([
            'type' => 'payment',
            'status' => 'created',
            'system' => 'advcash',
            'currency' => 'EUR',
            'to' => Yii::$app->user->identity->user,
            'ip' => Yii::$app->request->getUserIP(),
            'amount' => (int)$amount
        ]);
        $transfer->save(false);
        return $this->render('pay', [
            'id' => $transfer->id,
            'amount' => $amount / 100,
            'description' => ''
        ]);
    }
}
