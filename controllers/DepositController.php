<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\Deposit;
use Yii;
use yii\web\NotFoundHttpException;

class DepositController extends RestController
{
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index']
            ]
        ];
    }

    public function actionIndex() {
        $amount = +Yii::$app->request->post('amount');
        $program =  +Yii::$app->request->post('id');
        if ($amount > 0 && $program > 0) {
            $node_id = Deposit::open($program, Yii::$app->user->identity->user, $amount);
            if (-404 === $node_id) {
                throw new NotFoundHttpException('Deposit not found');
            } else if (-402 === $node_id) {
                return $this->redirect(['transfer/index',
                    'nick' => Yii::$app->user->identity->nick,
                    'amount' => $amount
                ]);
            } else {
                return $this->redirect(['node/index',
                    'nick' => Yii::$app->user->identity->nick,
                    'id' => $node_id
                ]);
            }
        }
        $models = Deposit::find()->all();
        return $this->render('index', [
            'models' => $models
        ]);
    }
}
