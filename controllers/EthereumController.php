<?php

namespace app\controllers;


use app\behaviors\Access;
use app\models\Log;
use Yii;
use yii\web\Controller;
use yii\web\Response;

class EthereumController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['address'],
//                'admin' => ['index']
            ]
        ];
    }

    public function beforeAction($action) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        Access::verifyNick();
        return parent::beforeAction($action);
    }

    public function actionAddress() {
        $address = Yii::$app->ethereum->getUserAddress();
        Log::log('ethereum', 'address');
        return ['address' => $address];
    }
/*
    public function actionUnlock($address, $password = '') {
        return ['result' => Yii::$app->ethereum->unlockAccount($address, $password)];
    }

    public function actionIndex() {
        return ['result' => Yii::$app->ethereum->getListAccounts()];
    }
*/
}
