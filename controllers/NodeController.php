<?php

namespace app\controllers;


use app\behaviors\Access;
use app\helpers\SQL;
use app\models\NodeSummary;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\Response;

class NodeController extends Controller {
    public function behaviors() {
        return [
            'access' => [
                'class' => Access::class,
                'plain' => ['index', 'tree']
            ]
        ];
    }

    public function actionIndex() {
        $user = Yii::$app->user->identity->user;
        $provider = new ActiveDataProvider([
            'query' => NodeSummary::find()
                ->where(['user' => $user])
        ]);
        return $this->render('index', [
            'provider' => $provider
        ]);
    }

    protected function build(&$rows, &$root) {
        $children = [];
        foreach ($rows as $row) {
            if ($row['parent'] === $root['id']) {
                $this->build($rows, $row);
                $children[] = $row;
            }
        }
        $count = count($children);
        if ($count > 0) {
            $root['children'] = $children;
        }
        if ($count < 4) {
            $root['is_active'] = !!$root['user'];
        }
        return $root;
    }

    public function actionTree($id) {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $rows = SQL::queryAll('SELECT u.nick, m.* FROM matrix m LEFT JOIN "user" u ON m.user = u.id WHERE root = :id ORDER BY id', [':id' => $id]);
        assert(gettype($rows) === 'array', 'Invalid type');
        $size = count($rows);
        if ($size <= 0) {
            Yii::$app->response->statusCode = 404;
            return ['message' => 'Node not found'];
        }

        $root = null;
        foreach ($rows as $row) {
            if (0 === $row['level']) {
                $root = $row;
                break;
            }
        }
        if ($root) {
            return [
                'size' => $size,
                'result' => $this->build($rows, $root),
            ];
        }
        else {
            Yii::$app->response->statusCode = 500;
            return ['message' => 'Root not found'];
        }
    }
}
