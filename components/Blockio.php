<?php

namespace app\components;


use app\models\User;
use yii\base\Component;
use yii\httpclient\Client;

class Blockio extends Component {
    public $key;
    public $secret;
    public $version = 2;
    public $delay = 0.4;
    public $last;
    public $client;

    public function __construct(array $config = []) {
        parent::__construct($config);
        $this->client = new Client();
        $this->last = 0;
    }

    public function invoke($method, $params = []) {
//        echo $method;
        $delta = microtime(true) - $this->last;
        if ($delta < $this->delay) {
            $d = (int)(($this->delay - $delta) * 1000 * 1000);
            usleep($d);
        }
        $this->last = microtime(true);
        $params['api_key'] = $this->key;
        $r = $this->client->createRequest()
            ->setUrl("https://block.io/api/v2/$method/?" . http_build_query($params))
            ->send();
//        echo $r->content . "\n";
        return json_decode($r->content);
    }

    public function unarchive($labels) {
        $this->invoke('unarchive_addresses', ['labels' => implode(',', $labels)]);
    }

    public function getAddress($nick) {
        $params = ['label' => $nick];
        $exist = $this->invoke('get_address_by_label', $params);
        $address = null;
        if ('success' === $exist->status) {
            $this->unarchive([$nick]);
            $address = $exist->data->address;
        } else {
            $new = $this->invoke('get_new_address', $params);
            if ('success' === $new->status) {
                $address = $new->data->address;
            }
        }
        return $address;
    }

    public function getUserAddress($nick) {
        $user = User::find()
            ->where(['nick' => $nick])
            ->select(['id', 'nick', 'blockio'])
            ->one();
        if ($user && $user->blockio) {
            $this->unarchive([$nick]);
            return $user->blockio;
        }
        $address = $this->getAddress($nick);
        if ($user && $address) {
            $user->blockio = $address;
            $user->save(false);
        }
        return $address;
    }
}
