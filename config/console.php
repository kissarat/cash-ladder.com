<?php
require_once 'functions.php';

return extend('console', 'common', [
    'controllerNamespace' => 'app\console\controllers',
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationPath' => '@app/console/migrations',
//            'interactive' => true,
            'migrationNamespaces' => [
//                'yii\log\migrations',
                'app\console\migrations'
            ]
        ],
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache'
        ],
    ]
]);
