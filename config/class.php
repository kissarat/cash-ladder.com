<?php
require_once 'functions.php';

return local('class', [
    'yii\data\Pagination' => [
        'defaultPageSize' => 10,
        'pageSizeParam' => 'limit',
    ],
    'yii\data\Sort' => ['defaultOrder' => ['id' => SORT_DESC]],
]);
