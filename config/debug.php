<?php
$config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    'allowedIPs' => ['127.0.0.1', '193.169.80.152', '185.53.170.57', '::1'],
];
//$config['modules']['gii'] = 'yii\gii\Module';

$config['bootstrap'][] = 'debug';
