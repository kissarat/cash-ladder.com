<?php
require_once 'functions.php';
$webConfig = [
    'bootstrap' => ['log'],
    'defaultRoute' => 'home/default',

    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'login' => 'user/login',
                'login/<sponsor:[\w_]+>' => 'user/login',
                'sponsor/<sponsor:[\w_]+>' => 'user/signup',
                'register' => 'user/signup',
                'tariff/<nick>/<id>' => 'program/index',
                'tariffs/<nick>' => 'program/index',
                'articles/<nick>' => 'article/index',
                'articles' => 'article/index',
                'newbie/<nick>' => 'home/newbie',
                'contact/<id>' => 'user/contact',
                'profile/<nick>' => 'user/profile',
                '<type>-history/<nick>' => 'transfer/index',
                'history/<nick>' => 'transfer/index',
                'pay/<nick>' => 'transfer/pay',
                'withdraw/<nick>' => 'transfer/withdraw',
                'structure/<nick>/<page>' => 'structure/index',
                'children/<id>' => 'structure/children',
                'settings/<nick>/profile' => 'settings/profile',
                'settings/<nick>/wallets' => 'settings/wallets',
                'settings/<nick>/password' => 'settings/password',
                'logout/<nick>' => 'user/logout',
                'deposits/<nick>' => 'deposit/index',
                'newbie' => 'user/visit',
                'success-perfect-money' => 'perfect/success',
                'status-perfect-money' => 'perfect/status',
                'fail-perfect-money' => 'perfect/fail',
                'under-construction/<nick>' => 'home/construction',
                'under-construction' => 'home/construction',
                'address/<nick>/blockio' => 'blockio/address',
                'address/<nick>/ethereum' => 'ethereum/address',
                'reset/<code>' => 'user/reset',
                '~c' => 'home/coordinate',
                '~v/<uid>/<url>/<spend>' => 'home/visit',
                'admin' => 'home/admin',
                'page/<path>/<nick>' => 'article/view',
                'sitemap.xml' => 'home/sitemap',
            ],
        ],

        'request' => [
            'class' => 'app\Request',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],

        'errorHandler' => [
            'errorAction' => 'home/error',
        ],

        'session' => [
            'class' => 'yii\web\DbSession',
            'readCallback' => function ($fields) {
                $fields['id'] = session_id();
                return $fields;
            }
        ],

        'user' => [
            'identityClass' => 'app\models\Identity',
            'enableAutoLogin' => true,
            'enableSession' => true,
            'idParam' => 'id',
            'autoRenewCookie' => false,
            'authTimeout' => 30 * 24 * 3600
        ],
    ]
];

if (!YII_DEBUG) {
    $webConfig['components']['assetManager'] = [
        'bundles' => [
            'yii\web\JqueryAsset' => [
                'sourcePath' => null,
                'js' => [
                    '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
                ]
            ],
        ],
    ];
}

return extend('web', 'common', $webConfig);
