<?php

require_once 'functions.php';

return local('common', [
    'basePath' => ROOT,
    'bootstrap' => [],
    'language' => 'en',
    'charset' => 'utf-8',
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'nullDisplay' => '',
        ],

        'cache' => [
            'class' => 'yii\caching\DbCache'
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'logTable' => 'trace'
                ],
            ],
        ],

        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'app\components\JsonMessageSource'
                ]
            ]
        ],

        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'port' => 587,
                'encryption' => 'tls',
            ],
        ],

        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'pgsql:host=localhost;dbname=october',
            'username' => 'october',
            'password' => 'october',
            'charset' => 'utf8'
        ],

        'blockio' => [
            'class' => 'app\components\Blockio'
        ],

        'ethereum' => [
            'class' => 'app\components\Ethereum',
            'host' => 'http://localhost:8545',
        ],

        'perfect' => [
            'class' => 'app\components\Perfect'
        ],

        'payeer' => [
            'class' => \yarcode\payeer\Merchant::class
        ],

        'advcash' => [
            'class' => \yarcode\advcash\Merchant::class,
            'sciCurrency' => 'EUR'
        ]
    ],
    'params' => [
        'referralLinkOrigin' => 'http://localhost'
    ],
]);
